############################################################
# Usage: python mesh_visu.py data_file_basename number_of_els_to_plot 
#  					whether_to_plot_dofs output_name whether_to_number_the_els whether_to_numner_the_dofs

import sys
import numpy as np
import csv
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
fig, ax = plt.subplots()

data = sys.argv[1]

x = np.array(pd.read_csv(data + "_dofs.csv", header = None))
ords = np.array(pd.read_csv(data + "_elords.csv", header = None), dtype = int)
max_ord = ords.max()
min_ord = ords.min()

cmap = plt.cm.RdBu
cols = cmap(np.linspace(0,1,max_ord))

#print("Shape: " + str(x.shape))

patches = []
index = 0
for df in pd.read_csv(data + "_els.csv",sep=',', header = None, chunksize=1):
	arr = np.array(df.iloc[0,:])
	siz_of_arr = arr.shape[0]//2
	arr = arr.reshape((siz_of_arr,2))
	patches = patches + [arr]
	 
	 
ax.set_aspect('equal')

num_args = len(sys.argv)
if(num_args > 3): 
	show_dofs = int(sys.argv[3])
else:
	show_dofs = False
if(num_args > 2): 
	only_first_k_els = int(sys.argv[2])
else:
	only_first_k_els = len(patches)
	
if(num_args > 4): 
	output_name = sys.argv[4]
else:
	output_name = "py_plot.pdf"
	
if(num_args > 5): 
	number_elements = int(sys.argv[5])
else:
	number_elements = True

if(num_args > 6): 
	number_dofs = int(sys.argv[6])
else:
	number_dofs = False
	
if(num_args > 7): 
	pos_range = int(sys.argv[7])
else:
	pos_range = False
	
if(len(patches) < only_first_k_els): only_first_k_els = len(patches)

scal = 0.2
pos_x = scal * 0.1
pos_y = scal * 0.1

### Number Dofs
if(show_dofs and number_dofs):
	for i in range(x.shape[0]):
		coords = x[i]
		ax.text(x[i,0] + pos_x,x[i,1] + pos_y, str(i), fontsize=10)

### Number elenemts
if(number_elements):
	for i in range(only_first_k_els):
		el_type = patches[i].shape[0] - 1
		center = (0,0)
		if(el_type == 4): print("hoi")
		for j in range(el_type):
			center += patches[i][j]
		center /= el_type
		ax.text(center[0],center[1], str(i), fontsize=10)

if(show_dofs):
	plt.plot(*zip(*x), marker='o', color='black', markersize=4, ls='')


pt2 = [Polygon(patches[i], closed=True, facecolor=cols[ords[i]-1][0]) for i in range(only_first_k_els)]

a = 1.3
if max_ord == min_ord:
	p = PatchCollection(pt2, alpha = 0.4)
else:
	p = PatchCollection(pt2, alpha = 0.4, match_original=True)
	
p.set_edgecolor('black')
#p.set_facecolor('white')
ax.add_collection(p)
x_max = x.max(0)
x_min = x.min(0)
ax.set_xlim([x_min[0] - 0.1,x_max[0] + 0.1])
ax.set_ylim([x_min[1] - 0.1,x_max[1] + 0.1])
plt.savefig(output_name, bbox_inches='tight')

