

#include "src/header.hpp"

#define VISU


int main(){
	// Choose a mesh
	index_t num_dofs;
	index_t num_boundary_dofs;
	nodes_t nodes;
	
	const index_t max_order = 4;
	const index_t num_mesh_type = 3;
	
	Info emp_inf;
	
	for(index_t mesh_ct = 0; mesh_ct < num_mesh_type; ++mesh_ct){
		for(index_t el_order = 1; el_order <= (mesh_ct == 2 ? 1 : max_order); ++el_order){
			
			// Initialize meshes
			const index_t n_meshes = (mesh_ct == 2 ? 8 : 6);
			std::vector<mesh> meshes;
			std::vector<nodes_t> nodes_vec(n_meshes);
			std::vector<index_t> num_dofs_vec(n_meshes);
			std::vector<index_t> num_boundary_dofs_vec(n_meshes);			
			for(index_t i = 1; i <= n_meshes; ++i){
				if(mesh_ct == 0)
					add_to_meshes(meshes, nodes_vec, num_dofs_vec, num_boundary_dofs_vec, RadialMeshWrapper((1<<i)-1,6*(1<<i-1), el_order));
				else if(mesh_ct == 1)
					add_to_meshes(meshes, nodes_vec, num_dofs_vec, num_boundary_dofs_vec, RegTriaMeshWrapper(i, 6, el_order));
				else if(mesh_ct == 2)
					add_to_meshes(meshes, nodes_vec, num_dofs_vec, num_boundary_dofs_vec, HPMeshWrapper(i, 6));
			}
			
			// Initialize output vectors for plots
			vector_t h1_errs(n_meshes);
			vector_t l2_errs(n_meshes);
			vector_t n_dofs(n_meshes);
			vector_t n_els(n_meshes);
			
			for(index_t mesh_id = 0; mesh_id < n_meshes; ++mesh_id){
				
				mesh final_mesh = meshes[mesh_id];
				num_dofs = num_dofs_vec[mesh_id];
				num_boundary_dofs = num_boundary_dofs_vec[mesh_id];
				nodes = nodes_vec[mesh_id];
				
				std::cout << "\nUsing mesh with: \n";
				std::cout << "Number of elements: " << final_mesh.elements.size() << "\n";
				std::cout << "Number of dofs: " << num_dofs << "\n";
				std::cout << "On boundary: " << num_boundary_dofs << "\n\n";
				
								
#ifdef VISU
				// Do the visualization
				const std::string dat_path = "../Data/";
				const std::string plt_path = "../Plots/";
				const std::string def_mesh_file_name_orig = std::string("original_mesh_") + 
										std::to_string(mesh_ct) + "_" + std::to_string(mesh_id) + "_" + std::to_string(el_order);
				visualize_mesh_generic(final_mesh,  nodes, dat_path + def_mesh_file_name_orig);
				if(num_dofs < 10000) system(std::string("python ../mesh_visu.py " + dat_path + def_mesh_file_name_orig + " "+ 
									std::to_string(2 * final_mesh.elements.size()) + " 1 " + plt_path + def_mesh_file_name_orig + "_with_dofs.pdf 0").c_str());
				if(el_order == 1) system(std::string("python ../mesh_visu.py " + dat_path + def_mesh_file_name_orig + " "+ 
									std::to_string(2 * final_mesh.elements.size()) + " 0 " + plt_path + def_mesh_file_name_orig + ".pdf 0").c_str());
				std::cout << "Done visualizing " << def_mesh_file_name_orig << ".\n";
#endif		
			
				// Specify boundary conditions
				auto fct = [&](coords_t x){
					return std::exp(x(1))*std::sin(x(0));
				};
				
				auto exact_sol_grad = [](coords_t x){
					coords_t res;
					res << std::exp(x(1))*std::cos(x(0)), std::exp(x(1))*std::sin(x(0));
					return res;
				};
				vector_t u0(num_boundary_dofs), ux(num_boundary_dofs), uy(num_boundary_dofs);
				for(int i = 0; i < num_boundary_dofs; ++i){
					u0(i) = fct(nodes.col(i));	
				}
				std::cout << "Boundary vector initialized.\n";
				
				// Assemble matrix
				std::cout << "\nAssembling matrix for auxiliary problem... \n";
				sparseMatrix_t A00(num_dofs-num_boundary_dofs, num_dofs-num_boundary_dofs);
				sparseMatrix_t A0d(num_dofs-num_boundary_dofs, num_boundary_dofs);
				sparseMatrix_t Add(num_boundary_dofs, num_boundary_dofs);
				const Info emp_inf;
				assemble<2>(A00, A0d, Add, final_mesh, nodes, emp_inf);
				std::cout << "Done.\n";
				
				// Solve sparse system
				std::cout << "\nSolving LSE... \n";
				solver_t solver;
				solver.compute(A00);
				const vector_t mu = solver.solve(-A0d*u0);
				std::cout << "\nDone.\n";
				
				// Validation
				std::cout << "\nComputing errors... \n";
				vector_t full_sol(num_dofs);
				full_sol << u0, mu;
				compute_h1_error(final_mesh, nodes, full_sol, exact_sol_grad, fct, h1_errs(mesh_id), l2_errs(mesh_id));
				n_dofs(mesh_id) = num_dofs;
				n_els(mesh_id) = final_mesh.numEls;
				std::cout << "Done.\n";
			}
			
			std::string data_path = "../Data/";
			std::string ext = "_valid_type_" + std::to_string(mesh_ct) + "_ord_" + std::to_string(el_order);
			writeToFile(data_path + "h1_errs" + ext + ".txt", h1_errs);
			writeToFile(data_path + "l2_errs" + ext + ".txt", l2_errs);
			writeToFile(data_path + "n_dofs" + ext + ".txt", n_dofs);
			writeToFile(data_path + "n_els" + ext + ".txt", n_els);
			system(std::string("python ../plot_error.py " + ext).c_str());
			std::cout << "Done with mesh type " << std::to_string(mesh_ct) << " with element order " << std::to_string(el_order) << ".\n";
		}
	}
	return 0;
}
