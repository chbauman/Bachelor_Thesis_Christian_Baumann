import numpy
import matplotlib
import sys
import matplotlib.pyplot as plt

num_args = len(sys.argv)
if(num_args > 1): 
	name_ext = sys.argv[1]
else:
	name_ext = ""
	
els = False

# Load the data
data_path = '../Data/'
t_sol1 = numpy.loadtxt(data_path + 't_sol1' + name_ext + '.txt')
t_sol2 = numpy.loadtxt(data_path + 't_sol2' + name_ext + '.txt')
t_asse_1 = numpy.loadtxt(data_path + 't_asse_1' + name_ext + '.txt')
t_asse_dir = numpy.loadtxt(data_path + 't_asse_dir' + name_ext + '.txt')
n_dofs = numpy.loadtxt(data_path + 'n_dofs' + name_ext + '.txt')
if els:
	resolutions = n_els
else:
	resolutions = n_dofs

# Plot the errors

#plt.plot(n_dofs, t_sol1, "-o", label='n_dofs vs. t_sol1')
plt.plot(n_dofs, t_sol2, "-o", label='LSE solving')
plt.plot(n_dofs, t_asse_1, "-x", label='Assembly auxiliary problem')
plt.plot(n_dofs, t_asse_dir, "-x", label='Direct assembly')

plt.legend(loc = 2)
plt.xlabel('# of dofs')
plt.ylabel('Time / s')
plt.grid('on')

#plt.show()
plt.savefig("../Plots/Time" + name_ext + ".pdf", bbox_inches='tight')
