import numpy
import matplotlib
import sys
import matplotlib.pyplot as plt

num_args = len(sys.argv)
if(num_args > 1): 
	name_ext = sys.argv[1]
else:
	name_ext = ""
	
els = False

# Load the data
data_path = '../Data/'
errors_h1 = numpy.loadtxt(data_path + 'h1_errs' + name_ext + '.txt')
errors_l2 = numpy.loadtxt(data_path + 'l2_errs' + name_ext + '.txt')
n_els = numpy.loadtxt(data_path + 'n_els' + name_ext + '.txt')
n_dofs = numpy.loadtxt(data_path + 'n_dofs' + name_ext + '.txt')
if els:
	resolutions = n_els
else:
	resolutions = n_dofs

# Plot the errors
if els:
	plt.loglog(n_els, errors_h1, "-o", label='n_els vs. $H^1$-error')
	plt.loglog(n_els, errors_l2, "-x", label='n_els vs. $L^2$-error')
else:
	plt.loglog(n_dofs, errors_h1, "-o", label='n_dofs vs. $H^1$-error')
	plt.loglog(n_dofs, errors_l2, "-x", label='n_dofs vs. $L^2$-error')

# Now we want to approximate the convergence rate
convergencePolynomial = numpy.polyfit(numpy.log(resolutions), numpy.log(errors_h1), 1)
plt.loglog(resolutions, numpy.exp(convergencePolynomial[1])*resolutions**convergencePolynomial[0], '--', label='$\mathcal{O}(N^{%.2f})$' % convergencePolynomial[0])
convergencePolynomial = numpy.polyfit(numpy.log(resolutions), numpy.log(errors_l2), 1)
plt.loglog(resolutions, numpy.exp(convergencePolynomial[1])*resolutions**convergencePolynomial[0], '--', label='$\mathcal{O}(N^{%.2f})$' % convergencePolynomial[0])
plt.legend(loc = 3)
plt.xlabel('$N$')
plt.ylabel('Error')
plt.grid('on')

#plt.show()

plt.savefig("../Plots/Errors" + name_ext + ".pdf", bbox_inches='tight')
