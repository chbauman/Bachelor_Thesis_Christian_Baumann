Instructions

The files in this folder produce code files
used for the main code. If 

	make
	
is executed, the file 'quadrature_tria.hpp'
in 'src' will be rewritten. To produce
'quadrature_quad.hpp', 'Quadrature.nb' needs
to be executed. The path telling the code
where to store it needs to be adjusted.

'ShapeFunctions.nb' will produce
the files 'shape_functions_tria.hpp' 
and 'shape_functions_quad.hpp' if executed.
