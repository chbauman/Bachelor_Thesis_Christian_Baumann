#include <iostream>
#include <iomanip>
#include <cstring>

#include "triangle_dunavant_rule.cpp"

int main(){
	std::cout << "Creating quadrature code...\n";
	/// Open file
	std::ofstream file("../src/quadrature_tria.hpp");
	file << std::setprecision(17);
	
	/// Header of file and start of function
	file << "// Automatically generated file, do not modify!!\n";
	file << "\n";
	file << "quadRule tria_qr(const quad_ord_t k){\n";
	
	const int num_rules = dunavant_rule_num();
	for(unsigned int i = 1; i <= num_rules; ++i){
		
		const int order_num = dunavant_order_num(i);
		
		if(i == 1)
			file << "\tif(k == 1){\n";
		else {
			file << "\t} else if(k == " << i << "){ // # of points: " << order_num << "\n";
		}
		file << "\t\tquad_weights_t weights(" << order_num << ");\n";
		file << "\t\tquad_points_t points(" << order_num << ",2);\n";

		double q_points[2*order_num] ;
		double q_weights[order_num];

		dunavant_rule(i, order_num, q_points, q_weights);
		
		/// weights
		for(unsigned int order = 0; order < order_num; order++){
			file << (order == 0? "\t\tweights <<  ":"\t\t\t\t\t");
			file << q_weights[order];
			file << (order != order_num - 1? ",\n":";\n");
		}
		
		/// points
		for(unsigned int order = 0; order < order_num; order++){
			file << (order == 0? "\t\tpoints <<  ":"\t\t\t\t\t");
			file << q_points[2*order] << "," << q_points[2*order+1];
			file << (order != order_num - 1? ",\n":";\n");
		}
		file << "\t\treturn quadRule(3, " << order_num << ", weights, points);\n";
	}
	
	/// End of function
	file << "\t} else {\n";
	file << "\t\tstd::cout << \"Quadrature not implemented for k = \" << k << \" in triangle.\\n\";\n";
	file << "\t}\n";
	file << "};\n";
	return 0;
}
