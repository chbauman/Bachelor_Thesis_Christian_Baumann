Instructions

To produce the some plots appearing in 
the thesis, execute

	make

again the 'Makefile' needs to be changed
if Eigen is not installed system wide.
If this is done one can also execute

	make all_plots
	
to get some nice plots of the shape functions
used in the thesis.
