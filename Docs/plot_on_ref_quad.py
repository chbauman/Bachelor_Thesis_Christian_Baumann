

import sys
import re
import numpy as np
import pandas as pd
import csv

data = sys.argv[1]

order = int(data[1])

dat = np.array(pd.read_csv(data, header = None))

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.cm as cm

x = np.array(dat[:,0], dtype = float)
y = np.array(dat[:,1], dtype = float)
z = np.array(dat[:,2], dtype = float)

fig = plt.figure()
ax = fig.gca(projection='3d')

ax.plot_trisurf(x, y, z, linewidth=0.0, cmap=cm.RdBu)

dof_dat_name = "quad_REF_" + str(order)

if data[3].isdecimal():
	nmd = int(data[3])
	dof_dat_name = dof_dat_name + "_" + data[3]
else: 
	dof_dat_name = dof_dat_name
	nmd = 0
	
active_dof = int(re.findall("[-+]?\d+[\.]?\d*[eE]?[-+]?\d*", data)[1 + nmd])

if nmd != 2:
	dof_dat = np.array(pd.read_csv(dof_dat_name + "_QuadEl_dofs.csv", header = None))
	
	#print(dof_dat.shape)
	
	for k in range(dof_dat.shape[0]):
		ax.plot([dof_dat[k,0]], [dof_dat[k,1]], [0], markerfacecolor='k', markeredgecolor='k', marker='o', markersize=5, alpha=0.6)
	
	ax.plot([dof_dat[active_dof,0]], [dof_dat[active_dof,1]], [1], markerfacecolor='k', markeredgecolor='k', marker='o', markersize=10, alpha=0.6)

ax.view_init(25, -120)


plt.savefig("../Plots/" + data + ".pdf", format='eps', dpi=1000)
