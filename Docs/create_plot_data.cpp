#include <Eigen/Dense>
#include <Eigen/SparseLU>
#include <Eigen/SparseCholesky>
#include <iostream>
#include <iomanip>
#include <random>

#include "../src/header.hpp"

const std::string dat_path = "../../Data/";
const std::string plt_path = "../../Plots/";

/// Create data to visualize function on reference element
template <index_t n_eval_per_side, class Function>
void create_bf_visu_data(Function & func_to_visu, const index_t indx, const std::string file_name = std::string("BF_"), const index_t el_type = 3){
	const int n_eval_tot = (el_type == 3? n_eval_per_side * (n_eval_per_side + 1) / 2 : n_eval_per_side * n_eval_per_side);
	/// 3 Vectors needed here because of size limit of matrices
	Eigen::Matrix<numeric_t, -1, 1>  plot_points_x(n_eval_tot);
	Eigen::Matrix<numeric_t, -1, 1>  plot_points_y(n_eval_tot);
	Eigen::Matrix<numeric_t, -1, 1>  plot_points_z(n_eval_tot);
	
	/// Triangle
	coords_t c1; c1 << 1, 0;
	coords_t c2; c2 << 0, 1;
	
	index_t ct = 0;
	for(index_t i = 0; i < n_eval_per_side; ++i){
		for(index_t j = 0; j < n_eval_per_side - (el_type == 3 ? i : 0); ++j){
			const coords_t point = 1.0 * i / (n_eval_per_side - 1) * c1 + 1.0 * j / (n_eval_per_side - 1) * c2;
			plot_points_x(ct) = point.transpose()(0);
			plot_points_y(ct) = point.transpose()(1);
			plot_points_z(ct) = func_to_visu(point)(indx);
			ct++;
		}
	}
	
	std::ofstream file(file_name + std::to_string(indx) + (el_type == 3 ? std::string("Tria") : 
					std::string("Quad")) + std::string("_VisuData.csv"));
	file << std::fixed;
	for(index_t i = 0; i < n_eval_tot; ++i){
		for(index_t j = 0; j < 1; ++j){
			//file << plot_points(i, j) << ", ";
			file << plot_points_x(i) << ", ";
			file << plot_points_y(i) << ", ";
			file << plot_points_z(i);
		}
		file << "\n";
	}
	
}

/// Create a mesh of the triangular reference element only
void create_tria_REF_mesh_visu_data(const index_t order, index_t num_missing_dofs = 0){			
	element_vec_t ref_mesh;
	nodes_t corner_dofs_tria(2,3);
	corner_dofs_tria << 0,1,0,
						0,0,1;
	nodes_t nodes = get_dofs_high_order(order, corner_dofs_tria);
	index_t n_tria_nodes = nodes.cols();
	if(num_missing_dofs > 0){
		nodes_t nd2(2,n_tria_nodes - num_missing_dofs);
		nodes_t smaller_order_nodes = get_dofs_high_order(order - 1, corner_dofs_tria);
		for(index_t l = 0; l < order - 1; ++l){
			nd2.col(l) = smaller_order_nodes.col(l);
		}
		for(index_t l = order - 1; l < n_tria_nodes - 1; ++l){
			nd2.col(l) = nodes.col(l + 1);
		}
		nodes = nd2;
	}
	index_vec_t index_vec_tria(n_tria_nodes);
	for(index_t i = 0; i < n_tria_nodes; ++i){
		index_vec_tria(i) = i;
		
	}
	std::string exten = std::string("_TriaEl");
	if(num_missing_dofs == 1) {
		exten = std::string("_1") + exten;
	}
	ref_mesh.push_back(element(3, index_vec_tria, order, num_missing_dofs));
	mesh ref_tria = mesh(ref_mesh, n_tria_nodes, order*3-2);
	visualize_mesh_generic(ref_tria, nodes, dat_path + std::string("tria_REF_") + std::to_string(order) + exten);

}



/// Create a mesh of the quadrilateral reference element only
void create_quad_REF_mesh_visu_data(const index_t order, index_t num_missing_dofs = 0){			
	element_vec_t ref_mesh;
	nodes_t corner_dofs_quad(2,4);
	corner_dofs_quad << 0,1,0,1,
						0,0,1,1;
	std::string exten = std::string("_QuadEl");
	
	nodes_t nodes = get_dofs_high_order(order, corner_dofs_quad);
	index_t n_quad_nodes = nodes.cols();
	if(num_missing_dofs > 0){
		nodes_t nd2(2,n_quad_nodes - num_missing_dofs);
		nodes_t smaller_order_nodes = get_dofs_high_order(order - 1, corner_dofs_quad);
		for(index_t l = 0; l < order - 1; ++l){
			nd2.col(l) = smaller_order_nodes.col(l);
		}
		for(index_t l = order - 1; l < n_quad_nodes - 1; ++l){
			nd2.col(l) = nodes.col(l + 1);
		}
		nodes = nd2;
		exten = std::string("_1") + exten;
	}
	
	index_vec_t index_vec_quad(n_quad_nodes);
	for(index_t i = 0; i < n_quad_nodes; ++i){
		index_vec_quad(i) = i;
	}
	ref_mesh.push_back(element(4, index_vec_quad, order, num_missing_dofs));
	mesh ref_quad = mesh(ref_mesh, n_quad_nodes, order*3-2);
	visualize_mesh_generic(ref_quad, nodes, dat_path + std::string("quad_REF_") + std::to_string(order) + exten);
}

mesh hip_offs(nodes_t & nodes){
	const index_t n_tria_in_center = 7;
	const index_t num_layers = 2;
	const numeric_t dr = 1.0 / (num_layers + 1);
	index_vec_t l8(7);
	l8 << 2,3,2,3,2,3,2;
	index_vec_t l9(3);
	l9 << 2,1,2;
	auto r = [&](double x){
		for(index_t i = 1; i <= num_layers; ++i){
			if(x < (i + 0.5) * dr){
				index_t temp = 1 << i;
				return 1.0 * (temp - 1.0) / temp; 
			}
		}
		return 1.0;
	};	
	std::vector<Layer> layer_vec;		
	layer_vec.push_back(Layer(0.5 ,l9));
	layer_vec.push_back(Layer(0.0 ,l8));
	
	int N = layer_vec.size();
	index_vec_t orders(N);
	orders.setConstant(1);
	orders *= 1;
	orders(N - 1) = 1;

	return construct_mesh(layer_vec, orders, n_tria_in_center, nodes, r);
}

int main(){
	index_t num_dofs;
	index_t num_boundary_dofs;	
	nodes_t nodes;	
	
	/// Meshes
	mesh h_msh = hp_mesh(nodes, 3, 7);
	visualize_mesh_generic(h_msh, nodes, dat_path + std::string("hp_3_7"));
	system(std::string("python ../../mesh_visu.py " + dat_path + "hp_3_7 7 0 " + plt_path + "center_construct_plot.pdf").c_str());
	system(std::string("python ../../mesh_visu.py " + dat_path + "hp_3_7 10 0 " + plt_path + "first_brick_hp_construct_plot.pdf").c_str());
	system(std::string("python ../../mesh_visu.py " + dat_path + "hp_3_7 28 0 " + plt_path + "first_layer_hp_construct_plot.pdf").c_str());
	system(std::string("python ../../mesh_visu.py " + dat_path + "hp_3_7 300 1 " + plt_path + "full_hp_construct_plot.pdf 0").c_str());

	mesh h_msh_offs = hip_offs(nodes);
	visualize_mesh_generic(h_msh_offs, nodes, dat_path + std::string("hp_3_7_offs"));
	system(std::string("python ../../mesh_visu.py " + dat_path + "hp_3_7_offs 10 0 " + plt_path + "first_brick_hp_offset_construct_plot.pdf").c_str());
	system(std::string("python ../../mesh_visu.py " + dat_path + "hp_3_7_offs 28 0 " + plt_path + "first_layer_hp_offset_construct_plot.pdf").c_str());
	
	mesh rad_msh = radial_mesh(nodes, 3, 1, 12);
	visualize_mesh_generic(rad_msh, nodes, dat_path + std::string("rad_3_16"));
	system(std::string("python ../../mesh_visu.py " + dat_path + "rad_3_16 300 1 " + plt_path + "full_rad_construct_plot.pdf 0").c_str());
		
	auto r_unif = [](double x){return x;};
	auto r_sin = [](double x){return sin(M_PI * x / 2);};
	mesh tria_msh_unif = triangular_mesh(nodes, 3, 7, r_unif);
	visualize_mesh_generic(tria_msh_unif, nodes, dat_path + std::string("tria_3_7_unif"));
	system(std::string("python ../../mesh_visu.py " + dat_path + "tria_3_7_unif 300 0 " + plt_path + "full_tria_unif_construct_plot.pdf 0").c_str());

	mesh tria_msh_sin = triangular_mesh(nodes, 3, 7, r_sin);
	visualize_mesh_generic(tria_msh_sin, nodes, dat_path + std::string("tria_3_7_sin"));
	system(std::string("python ../../mesh_visu.py " + dat_path + "tria_3_7_sin 300 0 " + plt_path + "full_tria_sin_construct_plot.pdf 0").c_str());
	mesh tria_msh = triangular_mesh(nodes, 5, 10, r_sin);
	visualize_mesh_generic(tria_msh, nodes, dat_path + std::string("tria_5_10"));
	system(std::string("python ../../mesh_visu.py " + dat_path + "tria_5_10 3000 1 " + plt_path + "full_tria_construct_plot.pdf 0").c_str());

	/// Shape functions
	for(index_t i = 1; i <= 6; ++i){
		create_tria_REF_mesh_visu_data(i);
		system(std::string("python ../../mesh_visu.py " + dat_path + "tria_REF_" + std::to_string(i) + "_TriaEl 1 1 " 
						+ plt_path + "tria_REF_" + std::to_string(i) + "_TriaEl_plot.pdf 0 1 1").c_str());
		if(i > 1) {
			create_tria_REF_mesh_visu_data(i, 1);
			system(std::string("python ../../mesh_visu.py " + dat_path + "tria_REF_" + std::to_string(i) + "_1_TriaEl 1 1 " 
						+ plt_path + "tria_REF_" + std::to_string(i) + "_1_TriaEl_plot.pdf 0 1 1").c_str());
		}
		create_quad_REF_mesh_visu_data(i);
		system(std::string("python ../../mesh_visu.py " + dat_path + "quad_REF_" + std::to_string(i) + "_QuadEl 1 1 " 
						+ plt_path + "quad_REF_" + std::to_string(i) + "_QuadEl_plot.pdf 0 1 1").c_str());
		if(i > 1) {
			create_quad_REF_mesh_visu_data(i, 1);
			system(std::string("python ../../mesh_visu.py " + dat_path + "quad_REF_" + std::to_string(i) + "_1_QuadEl 1 1 " 
						+ plt_path + "quad_REF_" + std::to_string(i) + "_1_QuadEl_plot.pdf 0 1 1").c_str());
		}
	}
	
	std::cout << "\nGenerating data for shape functions...\n";
	const index_t resol = 111;	
	
	for(index_t i = 1; i <= 4; ++i){
		const index_t n_tria_dofs = (i + 1) * (i + 2) / 2;
		for(index_t j = 0; j < n_tria_dofs; ++j){
			if(j < i && i > 1) {
				create_bf_visu_data<resol>(TriaBFs[2*(i-1)-1], j, dat_path + "b" + std::to_string(i) + "_1_tria");
			}
			create_bf_visu_data<resol>(TriaBFs[2*(i-1)], j, dat_path + "b" + std::to_string(i) + "_tria");
		}
		const index_t n_quad_dofs = (i + 1) * (i + 1);
		for(index_t j = 0; j < n_quad_dofs; ++j){
			if(j < i && i > 1) {
				create_bf_visu_data<resol>(QuadBFs[2*(i-1)-1], j, dat_path + "b" + std::to_string(i) + "_1_quad", 4);
			}
			create_bf_visu_data<resol>(QuadBFs[2*(i-1)], j, dat_path + "b" + std::to_string(i) + "_quad", 4);
		}
	}
	std::cout << "Done.\n\n";
}
	
