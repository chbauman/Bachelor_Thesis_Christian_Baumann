
#define VISU
//#define NA_DEF
//#define USE_DIRECT

#include "src/header.hpp"
#include "src/timer.hpp"

int main(){
	std::cout << std::setprecision(16);
	
	// Initialize mesh
	index_t num_dofs;
	index_t num_boundary_dofs;
	nodes_t nodes;
	
	// Choose some simple deformation
	const Deformation simple_def = get_deformations_vector()[0];
	
	
	const index_t tot_n_studies = 4;
	
	for(index_t msh_tpe = 1; msh_tpe <= tot_n_studies; msh_tpe++){
		
		const index_t n_meshes = (msh_tpe == tot_n_studies ? 8 : 6);	
		std::vector<mesh> meshes;
		std::vector<nodes_t> nodes_vec(n_meshes);
		std::vector<index_t> num_dofs_vec(n_meshes);
		std::vector<index_t> num_boundary_dofs_vec(n_meshes);	
	
		for(index_t i = 1; i <= n_meshes; ++i){
			//add_to_meshes(meshes, nodes_vec, num_dofs_vec, num_boundary_dofs_vec, RadialMeshWrapper((1<<i)-1,6*(1<<i-1), 3));
			//add_to_meshes(meshes, nodes_vec, num_dofs_vec, num_boundary_dofs_vec, HPMeshWrapper(i));
			//add_to_meshes(meshes, nodes_vec, num_dofs_vec, num_boundary_dofs_vec, TriangularMeshWrapper(3*i, 4*i));
			if(msh_tpe < tot_n_studies) add_to_meshes(meshes, nodes_vec, num_dofs_vec, num_boundary_dofs_vec, 
													RegTriaMeshWrapper(i, 6, msh_tpe));
			else add_to_meshes(meshes, nodes_vec, num_dofs_vec, num_boundary_dofs_vec, HPMeshWrapper(i, 6));
		}
		
		vector_t h1_errs(n_meshes);
		vector_t l2_errs(n_meshes);
		vector_t n_dofs(n_meshes);
		vector_t n_els(n_meshes);
		vector_t time_for_first_assembly(n_meshes);
		vector_t time_for_direct_assembly(n_meshes);
		vector_t time_for_solving_first(n_meshes);
		vector_t time_for_solving_second(n_meshes);
		
		std::cout << "\nMeshes constructed.\n";
		
		timep t_start, t_stop;
		
		for(index_t mesh_id = 0; mesh_id < n_meshes; ++mesh_id){
				
			mesh final_mesh = meshes[mesh_id];
			num_dofs = num_dofs_vec[mesh_id];
			num_boundary_dofs = num_boundary_dofs_vec[mesh_id];
			nodes = nodes_vec[mesh_id];
			
			std::cout << "\nUsing mesh with: \n";
			std::cout << "Number of elements: " << final_mesh.elements.size() << "\n";
			std::cout << "Number of dofs: " << num_dofs << "\n";
			std::cout << "On boundary: " << num_boundary_dofs << "\n\n";
			
			// Define parameters
			const index_t N = simple_def.n;
			Eigen::Matrix<numeric_t, -1, 1> s, c, y, z;
			s = simple_def.s;
			c = simple_def.c;
			y = simple_def.y;
			z = simple_def.z;
			
			auto r = [&](numeric_t phi){
				numeric_t res = 1.0;
				for(index_t i = 0; i < N; ++i){
					res += c(i) * y(i) * std::cos((i + 1) * phi) + s(i) * z(i) * std::sin((i + 1) * phi);
				};
				return res;
			};
				
			// Verify conditions on s, c, y and z
			if(s.cwiseAbs().sum() + c.cwiseAbs().sum() > 0.5 || 
							y.cwiseAbs().maxCoeff() > 1.0 || z.cwiseAbs().maxCoeff() > 1.0) 
				std::cout << "Parameters do not fulfill conditions.\n\n";
			
			// Specify boundary conditions
			auto bcx = [&](coords_t x){
				numeric_t phi = atan2(x(1), x(0));
				return (r(phi) - 1) * std::cos(phi);
			};
			auto bcy = [&](coords_t x){
				numeric_t phi = atan2(x(1), x(0));
				return (r(phi) - 1) * std::sin(phi);
			}; 
			
			vector_t ux(num_boundary_dofs), uy(num_boundary_dofs);
			for(index_t i = 0; i < num_boundary_dofs; ++i){
				ux(i) = bcx(nodes.col(i));
				uy(i) = bcy(nodes.col(i));		
			}
			std::cout << "Boundary vectors and parameters initialized.\n";
			
			// Assemble matrix
			Info emp_inf;
			sparseMatrix_t A00(num_dofs-num_boundary_dofs, num_dofs-num_boundary_dofs);
			sparseMatrix_t A0d(num_dofs-num_boundary_dofs, num_boundary_dofs);
			sparseMatrix_t Add(num_boundary_dofs, num_boundary_dofs);
			t_start = tnow();
			assemble<0>(A00, A0d, Add, final_mesh, nodes, emp_inf);
			t_stop = tnow();
			time_for_first_assembly(mesh_id) = tperiod(t_start, t_stop);
			
			// Solve sparse system
			solver_t solver;
			t_start = tnow();
			solver.compute(A00);
			const vector_t wx = solver.solve(-A0d*ux);
			t_stop = tnow();
			time_for_solving_first(mesh_id) = tperiod(t_start, t_stop);// Only one LSE
			const vector_t wy = solver.solve(-A0d*uy);
			std::cout << "\nLSE for transformation solved.\n";
			
			const nodes_t nodes2 = nodes;
				
			// Specify boundary conditions for final problem
			vector_t u0(num_boundary_dofs);
			auto fct = [](coords_t x){
				return std::exp(x(1))*std::sin(x(0));
			};		
			auto exact_sol_grad = [](coords_t x){
				coords_t res;
				res << std::exp(x(1))*std::cos(x(0)), std::exp(x(1))*std::sin(x(0));
				return res;
			};
			for(index_t i = 0; i < num_boundary_dofs; ++i){
				u0(i) = fct(nodes.col(i));	
			}
			
			// Assemble matrices for final problem
			std::cout << "\nAssembling final matrix...";
	
			nodes_t w_vec(2, num_dofs);
			w_vec << ux.transpose(), wx.transpose(), uy.transpose(), wy.transpose();
			const Info inf = Info(w_vec);
			t_start = tnow();
			assemble<1>(A00, A0d, Add, final_mesh, nodes2, inf);
			t_stop = tnow();
			time_for_direct_assembly(mesh_id) = tperiod(t_start, t_stop);
			
			std::cout << "Done for direct method.\nSolving LSE...";
			t_start = tnow();
			solver.compute(A00);
			const vector_t mu_dir = solver.solve(-A0d*u0);
			t_stop = tnow();
			time_for_solving_second(mesh_id) = tperiod(t_start, t_stop);
			std::cout << "Done for direct method.\n";
			
			n_dofs(mesh_id) = num_dofs;
			n_els(mesh_id) = final_mesh.numEls;

		};
		
		std::string data_path = "../Data/";
		std::string ext = (msh_tpe < tot_n_studies? "_const_tria_" + std::to_string(msh_tpe): "_hp");
		writeToFile(data_path + "h1_errs" + ext + ".txt", h1_errs);
		writeToFile(data_path + "l2_errs" + ext + ".txt", l2_errs);
		writeToFile(data_path + "n_dofs" + ext + ".txt", n_dofs);
		writeToFile(data_path + "n_els" + ext + ".txt", n_els);
		writeToFile(data_path + "t_sol1" + ext + ".txt", time_for_solving_first);
		writeToFile(data_path + "t_sol2" + ext + ".txt", time_for_solving_second);
		writeToFile(data_path + "t_asse_dir" + ext + ".txt", time_for_direct_assembly);
		writeToFile(data_path + "t_asse_1" + ext + ".txt", time_for_first_assembly);
		system(std::string("python ../plot_timing.py " + ext).c_str());
	}
	
	return 0;
}
