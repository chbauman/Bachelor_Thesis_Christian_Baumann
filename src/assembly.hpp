
// 0: trafo, 1: direct, 2: straight

/// Compute the determinants and check element deformation
template<class JAC_AT_QP, class DETS_AT_QP>
static inline void comp_dets(const index_t & qr_num_points, DETS_AT_QP & det_Dphi, const JAC_AT_QP & Dphi){
	for(index_t i = 0; i < qr_num_points; ++i){
		const numeric_t det = Dphi.template block<2,2>(i*2,0).determinant();
		if(det < 10e-10) std::cout << "Element has bad shape!!\n";
		det_Dphi(i) = std::abs(det);
	}
}

/// Transform the local gradients to the global gradients
template<index_t n_loc_dofs, class JAC_AT_QP, class GRADIENTS_AT_QP>
static inline void transf_loc_to_glob(const index_t & qr_num_points, 
				JAC_AT_QP & Dphi, GRADIENTS_AT_QP & Grads){
	for(index_t k = 0; k < qr_num_points; ++k){
		Dphi.template block<2,2> (2*k,0) = Dphi.template block<2,2> (2*k,0).inverse().transpose();
		for(index_t i = 0; i < n_loc_dofs; ++i){
			Grads.template block<2, 1>(k*2, i) = Dphi.template block<2,2> (2*k,0) * Grads.template block<2, 1>(k*2, i);
		}
	}
}

/// Assemble the local matrix 
template<index_t n_loc_dofs, class DETS_AT_QP, class GLO_GRADIENTS_AT_QP, class QUAD_WEIGHTS>
static inline lclMat_t assemble_elem_matrix_general(const index_t & qr_num_points, 
			const DETS_AT_QP & det_Dphi, const GLO_GRADIENTS_AT_QP & Grads, const QUAD_WEIGHTS & weights){
	lclMat_t lclMat(n_loc_dofs, n_loc_dofs);
	lclMat.setZero();
	for(index_t i = 0; i < n_loc_dofs; ++i){
		for(index_t j = 0; j < n_loc_dofs; ++j){
			numeric_t res = 0.0;
			for(index_t k = 0; k < qr_num_points; ++k){
				res += weights(k) * det_Dphi(k) * Grads.template block<2, 1>(k*2, i).dot(Grads.template block<2, 1>(k*2, j));
			}
			lclMat(i,j) += res;
		}
	}
	return std::move(lclMat);	
}


/// Global mapping of contributions of element matrix
template <class tripMat, class MESH, class Matrix, class NODES>
void mapContributions(tripMat & A00Trip, tripMat & A0dTrip, tripMat & AddTrip, const MESH & msh,
				const int indx, const Matrix & lclMat, const NODES & nds){
	const element el = msh.elements[indx];
	const unsigned short n = lclMat.cols();
	for(index_t i = 0; i < n; ++i){
		for(index_t j = 0; j < n; ++j){
			index_t dof1 = el.nodes(i), dof2 = el.nodes(j);
			const int nBd = msh.num_boundary_dofs;
			if(dof1 >= nBd){
				if(dof2 < nBd){
					A0dTrip.push_back(triplet_t(dof1 - nBd, dof2, lclMat(i,j)));
				} else {
					A00Trip.push_back(triplet_t(dof1 - nBd, dof2 - nBd, lclMat(i,j)));
				}
			} else if(dof2 < nBd) {
				AddTrip.push_back(triplet_t(dof1, dof2, lclMat(i,j)));
			};
		}
	}
}


#include "assembly_trafo.hpp"
#include "assembly_straight.hpp"
#include "assembly_direct.hpp"

struct Info {
	const nodes_t trafo_pointer;
	Info(nodes_t w_vec): trafo_pointer(w_vec) {};
	Info(){};
};


/// Some template meta programming to avoid long ugly code
template <el_type_t el_type, index_t asse_type, index_t N>
struct unroll_loop {
	template <typename NODES, typename ELEMENT, class INFO>
    static inline lclMat_t apply(const index_t order, const index_t nmd, const NODES & nds, const ELEMENT & el, const INFO & asse_info) {
        if(order == N){
			const index_t quad_number = quad_ord_to_use<N, el_type>();
			// 0: trafo, 1: direct, 2: straight
			if(asse_type == 0){
				if(nmd == 0) return assembleLocElMat_trafo<N,quad_number,0,el_type>(nds, el);
				else if(nmd == 1) return assembleLocElMat_trafo<N,quad_number,1,el_type>(nds, el);
			} else if(asse_type == 1){
				if(nmd == 0) return assembleLocElMat_trafo_dir<N,quad_number,0,el_type>(nds, el, asse_info);
				else if(nmd == 1) return assembleLocElMat_trafo_dir<N,quad_number,1,el_type>(nds, el, asse_info);
			} else if(asse_type == 2){
				if(nmd == 0) return assembleLocElMat_straight<N,quad_number,0,el_type>(nds, el);
				else if(nmd == 1) return assembleLocElMat_straight<N,quad_number,1,el_type>(nds, el);
			}
		}
		return unroll_loop<el_type,asse_type,N-1>::apply(order, nmd, nds, el, asse_info);  
    }
};

template <el_type_t el_type, index_t asse_type>
struct unroll_loop<el_type,asse_type,0> {
	template <typename NODES, typename ELEMENT, class INFO>
    static inline lclMat_t apply(const index_t order, const index_t nmd, const NODES & nds, 
					const ELEMENT & el, const INFO & asse_info) {
		std::cout << "Something went terribly wrong, check your code!\n";
    }
};

/// Element matrix assembly
template <index_t asse_type, class NODES, class MESH, class INFO>
lclMat_t assembleLocMat(const MESH & msh, const index_t indx, const NODES & nds, const INFO & asse_info){
	const element el = msh.elements[indx];
	const el_order_t order = el.order;
	const el_type_t el_t = el.el_type;
	const el_type_t nmd = el.num_missing_dofs;
	if(el_t == 3){
		// Triangular element
		if(order <= MAX_ORD) return 0.5 * unroll_loop<3,asse_type,MAX_ORD>::apply(order, nmd, nds, el, asse_info);
		std::cout << "Order too high, more quadrature rules needed.\n";
	} else if(el_t == 4) {
		// Quadrilateral element, order 1 to MAX_ORD
		if(order <= MAX_ORD) return unroll_loop<4,asse_type,MAX_ORD>::apply(order, nmd, nds, el, asse_info);
		std::cout << "Order too high, more quadrature rules needed.\n";
	} else {
		std::cout << "Only quadrilateral and triangular elements allowed.\n";
	};
};


/// Assembly of Global matrices A00, A0d and Add
template <index_t asse_type, class matrix, class MESH, class NODES, class INFO>
void assemble(matrix & A00, matrix & A0d, matrix & Add, const MESH & final_mesh,
				const NODES & nodes, const INFO & asse_info){
	tripletMatrix_t A00Trip, A0dTrip, AddTrip;
	const index_t n_els = final_mesh.numEls;
	for(index_t i = 0; i < n_els; ++i){
		// Assemble local matrix
		lclMat_t lclMat = assembleLocMat<asse_type>(final_mesh, i, nodes, asse_info);
		// Map contributions
		mapContributions(A00Trip, A0dTrip, AddTrip, final_mesh, i, lclMat, nodes);
	}
	A00.setFromTriplets(A00Trip.begin(), A00Trip.end());
	A0d.setFromTriplets(A0dTrip.begin(), A0dTrip.end());
	Add.setFromTriplets(AddTrip.begin(), AddTrip.end());
};
