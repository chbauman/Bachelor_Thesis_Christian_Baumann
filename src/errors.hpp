
template <index_t order, index_t quad_order, index_t num_missing_dofs, el_type_t el_type,
				class NODES, class ELEMENT, class GRADS, class BFS, class Function, class GradFunction, class QRFUN>
void get_el_error(const NODES & nds, const ELEMENT & el,
			const GRADS & grads, const BFS & bfs, const vector_t & fem_sol, 
			const GradFunction & exact_sol_grad, const Function & exact_sol, numeric_t & err_h1, 
			numeric_t & err_l2, const QRFUN & QRfuncs){
	
	const index_t n_loc_dofs = (el_type == 4 ? (order+1)*(order+1) : (order+1)*(order+2)/2) - num_missing_dofs;
	const quadRule QR = QRfuncs(quad_order);
	const quad_weights_t weights = QR.weights;
	const quad_points_t points = QR.points;
	const index_t qr_num_points = QR.n;
	
	numeric_t err_h1_new = 0.0;
	numeric_t err_l2_new = 0.0;
	
	/// Get the gradients and the values of the basis functions at the quadrature points
	Eigen::Matrix<numeric_t, -1, n_loc_dofs> Grads(2*qr_num_points, n_loc_dofs);
	Eigen::Matrix<numeric_t, -1, n_loc_dofs> Vals(qr_num_points, n_loc_dofs);
	for(index_t i = 0; i < qr_num_points; ++i){		
		Grads.template block<2, n_loc_dofs> (2*i, 0) = grads[2*(order-1) - num_missing_dofs](points.row(i)).transpose();
		Vals.template block<1, n_loc_dofs> (i, 0) = bfs[2*(order-1) - num_missing_dofs](points.row(i)).transpose();
	}
	
	/// Compute the transformation \phi from the shape functions
	Eigen::Matrix<numeric_t, -1, 2> Dphi_iso(2 * qr_num_points, 2); 
	Eigen::Matrix<numeric_t, -1, 1> phi_iso(2 * qr_num_points, 1); 
	Dphi_iso.setZero();
	phi_iso.setZero();
	for(index_t i = 0; i < n_loc_dofs; ++i){
		for(index_t k = 0; k < qr_num_points; ++k){
			Dphi_iso.template block<2,2> (2*k,0) += nds.col(el.nodes(i)) * Grads.template block<2,1>(2*k,i).transpose();
			phi_iso.template block<2,1> (2 * k,0) += nds.col(el.nodes(i)) * Vals(k,i);
		}
	}
	Eigen::Matrix<numeric_t, -1, 1> det_Dphi(qr_num_points, 1);
	for(index_t i = 0; i < qr_num_points; ++i){
		det_Dphi(i) = std::abs(Dphi_iso.template block<2,2>(i*2,0).determinant()) * weights(i);
	}
	
	/// Transform the local gradients to the global gradients
	for(index_t k = 0; k < qr_num_points; ++k){
		Dphi_iso.template block<2,2> (2*k,0) = Dphi_iso.template block<2,2> (2*k,0).inverse().transpose();
		for(index_t i = 0; i < n_loc_dofs; ++i){
			Grads.template block<2, 1>(k*2, i) = Dphi_iso.template block<2,2> (2*k,0) * Grads.template block<2, 1>(k*2, i);
		}
	}
	
	/// Compute the difference in gradients at each quadrature point
	Eigen::Matrix<numeric_t, -1, 1> Grads_tot(2*qr_num_points, 1);
	Eigen::Matrix<numeric_t, -1, 1> Val_tot(qr_num_points, 1);
	Grads_tot.setZero();
	Val_tot.setZero();
	for(index_t j = 0; j < qr_num_points; ++j){
		for(index_t i = 0; i < n_loc_dofs; ++i){
			Grads_tot.template block<2,1> (2*j, 0) += Grads.template block<2,1> (2*j, i) * fem_sol(el.nodes(i));
			Val_tot(j) += Vals(j, i) * fem_sol(el.nodes(i));
		}
		Grads_tot.template block<2,1> (2*j, 0) -= exact_sol_grad(phi_iso.template block<2,1> (2 * j,0));
		Val_tot(j) -= exact_sol(phi_iso.template block<2,1> (2 * j,0));
	}
	
	// Compute the integral using quadrature
	for(index_t j = 0; j < qr_num_points; ++j){
		err_h1_new += Grads_tot.template block<2,1> (2*j, 0).squaredNorm() * det_Dphi(j);
		err_l2_new += Val_tot(j) * Val_tot(j) * det_Dphi(j);
	}
	#pragma omp critical
	{
		err_h1 += (el_type == 4 ? err_h1_new : 0.5 * err_h1_new);
		err_l2 += (el_type == 4 ? err_l2_new : 0.5 * err_l2_new);
	}
};


// Some template meta programming to avoid long ugly code
template <el_type_t el_type, int N>
struct unroll_loop_error {
	template <typename NODES, typename ELEMENT, class GRADS, class BFS, class Function, class GradFunction, class QRFUN>
    static inline void apply(const index_t order, const index_t nmd, const NODES & nds, const ELEMENT & el,
				const GRADS & grads, const BFS & bfs, const vector_t & fem_sol, 
				const GradFunction & exact_sol_grad, const Function & exact_sol, numeric_t & err_h1, 
				numeric_t & err_l2, const QRFUN & QRfuncs) {
        if(order == N){
			const index_t quad_number = (el_type == 4 ? 8 : 12);
			if(nmd == 0) return get_el_error<N,quad_number,0,el_type>(nds, el, grads, bfs, fem_sol, 
						exact_sol_grad, exact_sol, err_h1, err_l2, QRfuncs);
			else if(nmd == 1) return get_el_error<N,quad_number,1,el_type>(nds, el, grads, bfs, fem_sol, 
						exact_sol_grad, exact_sol, err_h1, err_l2, QRfuncs);
		}
		unroll_loop_error<el_type,N-1>::apply(order, nmd, nds, el, grads, bfs, fem_sol, 
						exact_sol_grad, exact_sol, err_h1, err_l2, QRfuncs);
    }
};

template <el_type_t el_type>
struct unroll_loop_error<el_type, 0> {
	template <typename NODES, typename ELEMENT, class GRADS, class BFS, class Function, class GradFunction, class QRFUN>
    static inline void apply(const index_t order, const index_t nmd, const NODES & nds, const ELEMENT & el,
				const GRADS & grads, const BFS & bfs, const vector_t & fem_sol, 
				const GradFunction & exact_sol_grad, const Function & exact_sol, numeric_t & err_h1, 
				numeric_t & err_l2, const QRFUN & QRfuncs) {
		std::cout << "Something went wrong while computing the error, check your code!\n";
    }
};


/// Function that calculates the L2 and the H1 norm of the error between an exact solution
/// and the FEM solution
template <class GradFunction, class Function>
void compute_h1_error(const mesh & msh, const nodes_t & nodes, const vector_t fem_sol, const GradFunction & exact_sol_grad, const Function & exact_sol, numeric_t & err_h1, numeric_t & err_l2){
	
	index_t num_els = msh.numEls;
	err_h1 = 0.0;
	err_l2 = 0.0;
	
	/// Iterate over all elements
	#pragma omp parallel for
	for(index_t el_id = 0; el_id < num_els; ++el_id){
		const element curr_el = msh.elements[el_id];
		const index_t order = curr_el.order;
		const index_t el_t = curr_el.el_type;
		const index_t num_missing_dofs = curr_el.num_missing_dofs;
		if(el_t == 3){			
			if(order <= 9) unroll_loop_error<3,9>::apply(order, num_missing_dofs, nodes, curr_el, TriaGradients, TriaBFs, fem_sol, 
						exact_sol_grad, exact_sol, err_h1, err_l2, tria_qr);
		} else if (el_t == 4){
			if(order <= 9) unroll_loop_error<4,9>::apply(order, num_missing_dofs, nodes, curr_el, QuadGradients, QuadBFs, fem_sol, 
						exact_sol_grad, exact_sol, err_h1, err_l2, quad_qr);
		} else std::cout << "\nStrange element encountered.\n";
		
	}
	err_h1 = std::sqrt(err_h1);
	err_l2 = std::sqrt(err_l2);
}
	
