

// Assembly of element matrix of triangular element with shape functions of polynomial
// order "order" and quadrature order "quad_order".
template <unsigned int order, unsigned int quad_order, unsigned int num_missing_dofs, el_type_t el_type, 
				class NODES, class ELEMENT>
lclMat_t assembleLocElMat_trafo(const NODES & nds, const ELEMENT & el){
	const index_t n_loc_dofs = (el_type == 4 ? (order+1)*(order+1) : (order+1)*(order+2)/2) - num_missing_dofs;
	
	const quadRule QR = (el_type == 4 ? quad_qr(quad_order) : tria_qr(quad_order));
	const quad_weights_t weights = QR.weights;
	const quad_points_t points = QR.points;
	const index_t qr_num_points = QR.n;
	
	/// Get the gradients at the quadrature points
	Eigen::Matrix<numeric_t, -1, n_loc_dofs> Grads(2*qr_num_points, n_loc_dofs);
	for(index_t i = 0; i < qr_num_points; ++i){
		Grads.template block<2, n_loc_dofs> (2*i, 0) = 
						(el_type == 4 ? QuadGradients[2*(order-1) - num_missing_dofs](points.row(i)).transpose() : 
										TriaGradients[2*(order-1) - num_missing_dofs](points.row(i)).transpose());
	}
	
	/// Compute the Jacobians of the transformation \phi from the shape functions
	Eigen::Matrix<numeric_t, -1, 2> Dphi_iso(2 * qr_num_points,2);
	Dphi_iso.setZero();
	for(index_t i = 0; i < n_loc_dofs; ++i){
		for(index_t k = 0; k < qr_num_points; ++k){
			Dphi_iso.template block<2,2> (2*k,0) += nds.col(el.nodes(i)) * Grads.template block<2,1>(2*k,i).transpose();
		}
	}
	
	/// Compute the determinants and check element deformation
	Eigen::Matrix<numeric_t, -1, 1> det_Dphi(qr_num_points);
	comp_dets(qr_num_points, det_Dphi, Dphi_iso);
	
	/// Transform the local gradients to the global gradients
	transf_loc_to_glob<n_loc_dofs>(qr_num_points, Dphi_iso, Grads);
	
	/// Assemble the local matrix 
	return assemble_elem_matrix_general<n_loc_dofs>(qr_num_points, det_Dphi, Grads, weights);
};


