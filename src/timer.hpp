#ifndef TIMER_HPP_INCLUDED
#define TIMER_HPP_INCLUDED

#include <cmath>
#include <chrono>

typedef std::chrono::high_resolution_clock hrc;
typedef std::chrono::time_point<hrc> timep;

double tperiod(const timep t1, const timep t2){
    double d = std::chrono::duration_cast<std::chrono::duration<double>>(t1 - t2).count();
    return std::abs(d);
}
timep tnow(){
    return hrc::now();
}

#endif // TIMER_HPP_INCLUDED
