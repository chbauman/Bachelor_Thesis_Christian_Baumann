

// General procedure for assembling local matrices
template <index_t order, index_t quad_order, index_t num_missing_dofs, el_type_t el_type,
				class NODES, class ELEMENT, class INFO>
lclMat_t assembleLocElMat_trafo_dir(const NODES & nds, const ELEMENT & el, const INFO & asse_info){
	const index_t n_loc_dofs = (el_type == 4 ? (order+1)*(order+1) : (order+1)*(order+2)/2) - num_missing_dofs;
	
	const quadRule QR = (el_type == 4 ? quad_qr(quad_order) : tria_qr(quad_order));
	const quad_weights_t weights = QR.weights;
	const quad_points_t points = QR.points;
	const index_t qr_num_points = QR.n;
	
	/// Get the gradients and the values of the basis functions at the quadrature points
	Eigen::Matrix<numeric_t, -1, n_loc_dofs> Grads(2*qr_num_points, n_loc_dofs);
	Eigen::Matrix<numeric_t, -1, n_loc_dofs> Vals(qr_num_points, n_loc_dofs);
	for(index_t i = 0; i < qr_num_points; ++i){		
		Grads.template block<2, n_loc_dofs> (2*i, 0) = 
								(el_type == 4 ? QuadGradients[2*(order-1) - num_missing_dofs](points.row(i)).transpose() : 
										TriaGradients[2*(order-1) - num_missing_dofs](points.row(i)).transpose());
		Vals.template block<1, n_loc_dofs> (i, 0) = 
								(el_type == 4 ? QuadBFs[2*(order-1) - num_missing_dofs](points.row(i)).transpose() : 
										TriaBFs[2*(order-1) - num_missing_dofs](points.row(i)).transpose());
	}
	
	/// Compute the transformation \phi from the shape functions
	Eigen::Matrix<numeric_t, -1, 2> Dphi_iso(2 * qr_num_points, 2); 
	Eigen::Matrix<numeric_t, -1, 2> Dbig_phi_iso(2 * qr_num_points, 2); 
	Eigen::Matrix<numeric_t, -1, 1> phi_iso(2 * qr_num_points, 1); 
	Dphi_iso.setZero();
	Dbig_phi_iso.setZero();
	phi_iso.setZero();
	for(index_t i = 0; i < n_loc_dofs; ++i){
		for(index_t k = 0; k < qr_num_points; ++k){
			Dphi_iso.template block<2,2> (2*k,0) += nds.col(el.nodes(i)) * Grads.template block<2,1>(2*k,i).transpose();
			phi_iso.template block<2,1> (2 * k,0) += nds.col(el.nodes(i)) * Vals(k,i);
		}
	}
	for(index_t k = 0; k < qr_num_points; ++k){
		for(index_t i = 0; i < n_loc_dofs; ++i){
			Dbig_phi_iso.template block<2,2> (2*k,0) += asse_info.trafo_pointer.col(el.nodes(i)) * 
							Grads.template block<2,1>(2*k,i).transpose();
		}
		Dbig_phi_iso.template block<2,2> (2*k,0) += Dphi_iso.template block<2,2> (2*k,0);
	}
	
	/// Compute the determinants and check element deformation
	Eigen::Matrix<numeric_t, -1, 1> det_Dbig_phi(qr_num_points);
	comp_dets(qr_num_points, det_Dbig_phi, Dbig_phi_iso);
	
	/// Transform the local gradients to the global gradients
	transf_loc_to_glob<n_loc_dofs>(qr_num_points, Dbig_phi_iso, Grads);
	
	/// Assemble the local matrix 
	return assemble_elem_matrix_general<n_loc_dofs>(qr_num_points, det_Dbig_phi, Grads, weights);	
}

