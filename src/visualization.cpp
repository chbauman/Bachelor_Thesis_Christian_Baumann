

/// Code producing Mathematica code

template<class MESH, class NODES, class DATA_FILE>
void add_els(MESH & msh, NODES & nodes, DATA_FILE & file, const index_t & num_els){
	 for(index_t i = 0; i < num_els; ++i) {
        file << "\nLine[{";
        for(index_t k = 0; k < msh.elements[i].el_type; ++k){
			coords_t cur_node = nodes.col(msh.elements[i].nodes(msh.elements[i].corner(k)));
			file << "{" << cur_node(0) << ", " << cur_node(1) << "},";
		}
		coords_t last_node = nodes.col(msh.elements[i].nodes(msh.elements[i].corner(0)));
		file << "{" << last_node(0) 
				<< ", " << last_node(1) << "}";

        file << "}]" << (i == num_els - 1 ? "" : ",");
    }
}

void visualize_mesh(const mesh & msh, const nodes_t & nodes, int num_els_to_show){
	std::ofstream file("mesh_math.txt");
	if(nodes.cols() > 2000) std::cout << "\nVisualization of mesh of more than 2000 dofs.\n";
	
	file << std::fixed;
	index_t num_els = msh.numEls;
	file << "Graphics[{";
	
    if(num_els_to_show < 0) {
		index_t num_els = msh.numEls;
		add_els(msh, nodes, file, num_els);
	} else add_els(msh, nodes, file, num_els_to_show);
    
    file << "}, Frame->True]\n";
}



void visualize_mesh_showing_interior_dofs(const mesh & msh, const nodes_t & nodes){
	std::ofstream file("mesh_full_math.txt");
	if(nodes.cols() > 2000) std::cout << "\nVisualization of mesh of more than 2000 dofs.\n";
	
	file << std::fixed;
	index_t num_els = msh.numEls;
	file << "Graphics[{";
	
    for(index_t i = 0; i < num_els; ++i) {
        file << "\nLine[{";
        index_t el_tp = msh.elements[i].el_type;
        index_t ord = msh.elements[i].order;
        index_t n_df = (el_tp == 3 ? (ord + 1) * (ord + 2) / 2 : (ord + 1) * (ord + 1)) - msh.elements[i].num_missing_dofs;
        for(index_t k = 0; k < n_df; ++k){
			//std::cout << msh.elements[i].nodes(k) << "\n";
			coords_t cur_node = nodes.col(msh.elements[i].nodes(k));
			file << "{" << cur_node(0) << ", " << cur_node(1) << "}" << (k == n_df - 1 ? "" : ",");
		}
		//std::cout << "Hoi\n";
        file << "}]" << (i == num_els - 1 ? "" : ",");
    }
    file << "}, Frame->True]\n";
}

void visualize_mesh_with_dofs(const mesh & msh, const nodes_t & nodes){
	std::ofstream file("mesh_math.txt");
	if(nodes.cols() > 2000) std::cout << "\nVisualization of more than 2000 dofs.\n";
	
	file << std::fixed;
	index_t num_els = msh.numEls;
	file << "Graphics[{";
	
    add_els(msh, nodes, file, num_els);
    
	index_t num_dofs = nodes.cols();
	file << "PointSize[0.01],Point[{";
	
    for(index_t i = 0; i < num_dofs; ++i) {
		coords_t curr_dof = nodes.col(i);
		file << "{" << curr_dof(0) << ", " << curr_dof(1) << "}";

        file << (i == num_dofs - 1 ? "" : ",");
    }
    file << "}]}, Frame->True]\n";
}



/// Python plotting
template<class MESH, class NODES, class DATA_FILE>
void add_els_python(MESH & msh, NODES & nodes, DATA_FILE & file, const index_t & num_els, const index_t start_dof_id = 0){
	 for(index_t i = start_dof_id; i < num_els; ++i) {
        file << "\nnp.array([";
        for(index_t k = 0; k < msh.elements[i].el_type; ++k){
			coords_t cur_node = nodes.col(msh.elements[i].nodes(msh.elements[i].corner(k)));
			file << "(" << cur_node(0) << ", " << cur_node(1) << "),";
		}
		coords_t last_node = nodes.col(msh.elements[i].nodes(msh.elements[i].corner(0)));
		file << "(" << last_node(0) 
				<< ", " << last_node(1) << ")";

        file << "]),";
    }
    file << "\n]";
}

#include <string>

void visualize_mesh_python(const mesh & msh, const nodes_t & nodes, int num_els_to_show, bool show_all_dofs, const index_t start_dof_id = 0, const std::string file_name = "mesh_visu_data.py"){
	std::ofstream file(file_name);
	if(nodes.cols() > 2000) std::cout << "\nVisualization of mesh of more than 2000 dofs.\n";
	
	file << std::fixed;
	file << "import numpy as np\n\n";

	index_t num_els = msh.numEls;
	file << "patches = [";
	
    if(num_els_to_show < 0) {
		index_t num_els = msh.numEls;
		add_els_python(msh, nodes, file, num_els);
	} else add_els_python(msh, nodes, file, num_els_to_show, start_dof_id);
	
	if(show_all_dofs){
		index_t num_dofs = nodes.cols();
		file << "\nx = np.array([";
		
		for(index_t i = 0; i < num_dofs; ++i) {
			coords_t curr_dof = nodes.col(i);
			file << "\n[" << curr_dof(0) << ", " << curr_dof(1) << "],";
		}
		file << "\n])";
	}
    
}

/// Generic plotting
template<class MESH, class NODES, class DATA_FILE>
void add_els_generic(MESH & msh, NODES & nodes, DATA_FILE & file, const index_t & num_els, const index_t start_dof_id = 0){
	 for(index_t i = start_dof_id; i < num_els; ++i) {
        file << "";
        for(index_t k = 0; k < msh.elements[i].el_type; ++k){
			coords_t cur_node = nodes.col(msh.elements[i].nodes(msh.elements[i].corner(k)));
			file << "" << cur_node(0) << ", " << cur_node(1) << ",";
		}
		coords_t last_node = nodes.col(msh.elements[i].nodes(msh.elements[i].corner(0)));
		file << "" << last_node(0) 
				<< "," << last_node(1) << "";

        file << "\n";
    }
    file << "\n";
}

#include <string>
void visualize_mesh_generic(const mesh & msh, const nodes_t & nodes, const std::string file_name = "mesh_data"){
	std::ofstream file(file_name + std::string("_els.csv"));
	std::ofstream file2(file_name + std::string("_dofs.csv"));
	std::ofstream file3(file_name + std::string("_elords.csv"));
	if(nodes.cols() > 2000) std::cout << "\nVisualization of mesh of more than 2000 dofs.\n";
	
	file << std::fixed;
	file2 << std::fixed;

	index_t num_els = msh.numEls;
	
	add_els_generic(msh, nodes, file, num_els);
	
	for(index_t i = 0; i < num_els; ++i){
		file3 << msh.elements[i].order << "\n";
	}
	index_t num_dofs = nodes.cols();
	
	for(index_t i = 0; i < num_dofs; ++i) {
		coords_t curr_dof = nodes.col(i);
		file2 << curr_dof(0) << "," << curr_dof(1) << "\n";
	}

    
}

