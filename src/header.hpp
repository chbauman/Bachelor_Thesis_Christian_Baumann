#include <Eigen/Dense>
#include <Eigen/SparseLU>
#include <Eigen/SparseCholesky>
#include <iostream>
#include <iomanip>
#include <random>

typedef double numeric_t;
typedef unsigned int index_t;
typedef Eigen::Matrix<index_t, -1, 1> index_vec_t;
typedef Eigen::Triplet<numeric_t> triplet_t;
typedef Eigen::SparseMatrix<numeric_t> sparseMatrix_t;
typedef Eigen::Matrix<numeric_t, -1, 1> vector_t;
typedef std::vector<triplet_t> Triplets_t;
typedef Eigen::Matrix<numeric_t, 2, 1> coords_t;
typedef Eigen::SimplicialLDLT<sparseMatrix_t> solver_t;
typedef std::vector<triplet_t> tripletMatrix_t;
typedef Eigen::Matrix<numeric_t, 2, 2> coord_mat_t;
typedef Eigen::Matrix<numeric_t, -1, -1> lclMat_t;

#include "timer.hpp"
#include "mesh_container.hpp"
#include "quadrature.hpp"
#include "fe_basis.hpp"

#include "assembly.hpp"
#include "meshes.cpp"
#include "writer.hpp"
#include "visualization.cpp"
#include "errors.hpp"
#include "defs.hpp"

typedef Eigen::Matrix<numeric_t, 2, -1> nodes_t;

typedef unsigned short el_type_t;
typedef std::vector<element> element_vec_t;
