

typedef Eigen::Matrix<numeric_t, -1, 1> def_vec;
struct Deformation {
	const def_vec s;
	const def_vec c;
	const def_vec y;
	const def_vec z;
	const index_t n;
	const std::string name;
	Deformation(const def_vec s, const def_vec c, const def_vec y, const def_vec z, const std::string name):
				s(s), c(c), y(y), z(z), name(name), n(s.size()){};
};

std::vector<Deformation> get_deformations_vector(){
	// std::vector storing all deformations
	std::vector<Deformation> defs_to_try;

	// Small deformation
	defs_to_try.push_back(Deformation(Eigen::Vector2d(0.1, 0.05),
								Eigen::Vector2d(0.06, 0.12),
								Eigen::Vector2d(0.6, -0.8),
								Eigen::Vector2d(-0.4, 0.9), std::string("small")));
								
	// Some other deformation
	const index_t def_2_n = 100;
	Eigen::Matrix<numeric_t, def_2_n, 1> s_2, c_2, y_2, z_2;
	c_2.setZero();
	for(index_t i = 3; i < def_2_n; i+=4){
		c_2(i) = ((i+1)/4 % 2 ? -1: 1) * 2.999 * 16 / (i+1) / (i+1) / M_PI / M_PI;
	}
	s_2.setConstant(0.0);
	y_2.setConstant(1.0);
	z_2.setConstant(1.0);
	defs_to_try.push_back(Deformation(s_2,c_2,y_2,z_2, std::string("star")));

	// Peak towards interior --v--
	const index_t def_5_n = 100;
	Eigen::Matrix<numeric_t, def_5_n, 1> s_5, c_5, y_5, z_5;
	numeric_t dist = 0.15;
	c_5.setZero();
	for(index_t i = 0; i < def_5_n; i++){
		c_5(i) = -1.999 * (1. - cos(dist * (i + 1) * M_PI)) / (2. - dist) / dist / (i + 1) / (i + 1) / M_PI / M_PI;
	}
	s_5.setConstant(0.0);
	y_5.setConstant(1.0);
	z_5.setConstant(1.0);
	defs_to_try.push_back(Deformation(s_5,c_5,y_5,z_5, std::string("crack")));

/*
	// Some other deformation
	const index_t def_3_n = 100;
	Eigen::Matrix<numeric_t, def_3_n, 1> s_3, c_3, y_3, z_3;
	c_3.setZero();
	for(index_t i = 1; i < def_3_n; i+=4){
		c_3(i) = 3.9 / (i/2+1) / (i/2+1) / M_PI / M_PI;
	}
	s_3.setConstant(0.0);
	y_3.setConstant(1.0);
	z_3.setConstant(1.0);
	defs_to_try.push_back(Deformation(s_3,c_3,y_3,z_3, std::string("infinity")));
*/
	// Large deformation causing problems
	const index_t bad_def_n = 8;
	Eigen::Matrix<numeric_t, bad_def_n, 1> s_bad, c_bad, y_bad, z_bad;
	c_bad.setConstant(-1.0 / bad_def_n / 2.01);
	s_bad.setConstant(0.0);
	y_bad.setConstant(1.0);
	z_bad.setConstant(1.0);
	defs_to_try.push_back(Deformation(s_bad,c_bad,y_bad,z_bad, std::string("large_bad")));
	
	return defs_to_try;
};
