#include <fstream>


// Automatically generated meshes
// Mesh of unit circle
mesh unit_circle_tria_order_1(nodes_t & nodes, 
			index_t & num_dofs, index_t & num_boundary_dofs){
	
	const numeric_t fac = 1.0;
	auto f = [&](numeric_t x){
		return std::sin(fac*M_PI*x/2);
	};
	
	const int n_circles = 11;
	num_boundary_dofs = 64;
	index_vec_t desc(n_circles-1);
	desc << 1,1,1,2,1,1,2,1,2,2;
	
	numeric_t r_old = f(1.0 / n_circles);
	index_t el_edges_p_c = 4;
	for(index_t i = 1; i < n_circles; ++i){
		if(M_PI * 2 * f((i + 1.0)/n_circles) / el_edges_p_c / (f((i + 1.0)/n_circles) - f((1.0 * i)/n_circles)) > 1.2) {
			el_edges_p_c *= 2;
			desc(i - 1) = 2;
		} else desc(i - 1) = 1;
	}
	//std::cout << desc.transpose() << "<- desc\n";
	//std::cout << num_boundary_dofs << "<- nbd\n";
	
	num_boundary_dofs = el_edges_p_c;
	/*
	const int n_circles = 2;
	num_boundary_dofs = 8;
	index_vec_t desc(n_circles-1);
	desc << 2;
	*/
	
	index_vec_t dofs_per_circle(n_circles-1);
	index_t curr_ndof = num_boundary_dofs;
	num_dofs = num_boundary_dofs;
	for(index_t i = 0; i < n_circles-1; ++i){
		if(desc(i)==2) curr_ndof /= 2;
		dofs_per_circle(i) = curr_ndof;
		num_dofs += curr_ndof;
		
	}
	if(curr_ndof != 4) std::cout << "bad parameters!\n";
	nodes = Eigen::Matrix<numeric_t, 2, -1> (2,num_dofs);
	element_vec_t simple_mesh;
	
	for(index_t i = 0; i < num_boundary_dofs; ++i){
		nodes(0,i) = std::cos(2*M_PI*i/num_boundary_dofs);
		nodes(1,i) = std::sin(2*M_PI*i/num_boundary_dofs);
	};
	numeric_t offset = 0.0;
	index_t dof_count = num_boundary_dofs;
	index_t dof_count_old = 0;
	for(index_t i = 0; i < n_circles-1; ++i){
		index_t dpc = dofs_per_circle(i);
		if(desc(i) == 1) offset += M_PI/dpc;
		for(index_t k = 0; k < dpc; ++k){
			nodes(0,dof_count + k) = f(1 - (i+1.)/n_circles) * std::cos(2*M_PI*k/dpc + offset);
			nodes(1,dof_count + k) = f(1 - (i+1.)/n_circles) * std::sin(2*M_PI*k/dpc + offset);
			if(desc(i) == 2){
				index_vec_t el(3);
				el(0) = dof_count + k;
				el(1) = dof_count_old + 2*k;
				el(2) = dof_count_old + 2*k + 1;
				simple_mesh.push_back(element(3,el,1));
				el(0) = dof_count + k;
				el(1) = dof_count_old + 2*k + 1;
				if(k != dpc - 1) el(2) = dof_count + k + 1;
				else el(2) = dof_count;
				simple_mesh.push_back(element(3,el,1));
				if(k != dpc - 1) el(0) = dof_count + k + 1;
				else el(0) = dof_count;
				el(1) = dof_count_old  + 2*k + 1;
				if(k != dpc - 1) el(2) = dof_count_old  + 2*(k + 1);
				else el(2) = dof_count_old;
				simple_mesh.push_back(element(3,el,1));
			};
			if(desc(i) == 1){
				index_vec_t el(3);
				el(0) = dof_count + k; 
				el(1) = dof_count_old + k;
				if(k != dpc - 1) el(2) = dof_count_old+ k + 1;
				else el(2) = dof_count_old;
				simple_mesh.push_back(element(3,el,1));
				el(0) = dof_count + k; 
				if(k != dpc - 1) {
					el(1) = dof_count_old + k + 1;
					el(2) = dof_count + k+1;
				}
				else {
					el(1) = dof_count_old;
					el(2) = dof_count;
				}
				simple_mesh.push_back(element(3,el,1));
			}
		}
		dof_count_old = dof_count;
		dof_count += dpc;
	}
	index_vec_t el(3);
	el(0) = dof_count_old;
	el(1) = dof_count_old +1;
	el(2) = dof_count_old +3;
	simple_mesh.push_back(element(3,el,1));
	el(0) = dof_count_old + 3;
	el(1) = dof_count_old +1;
	el(2) = dof_count_old +2;
	simple_mesh.push_back(element(3,el,1));
	return mesh(simple_mesh, num_dofs, num_boundary_dofs);
};


/// Assuming const order el.
nodes_t get_dofs_high_order(el_order_t el_order, nodes_t corner_dofs){
	index_t el_type = corner_dofs.cols();
	index_t num_dofs = get_num_dofs(el_type, el_order);
	nodes_t res(2, num_dofs);
	
	coords_t base_corner; base_corner << 0.,0.;
	coords_t d1; d1 << 1.,0.; d1 /= el_order;
	coords_t d2; d2 << 0.,1.; d2 /= el_order;
	
	//std::cout << "\nd1: " << d1.transpose() << " d2: " << d2.transpose() << "\n";
	index_t counter = 0;
	for(index_t i = 0; i < el_order + 1; ++i){
		for(index_t k = 0; k < el_order - (el_type == 4 ? 0 : i) + 1; ++k){
			res.col(counter) = base_corner + k * d1 + i * d2;
			//std::cout << "Dof: " << res.col(counter).transpose();
			counter ++;
		}
	}
	//std::cout << "\nRes: \n" << res;
	//std::cout << "\ncorner_dofs \n" << corner_dofs;
	for(index_t i = 0; i < num_dofs; ++i){
		if(el_type == 3) res.col(i) = aff_trafo(res.col(i), corner_dofs.col(0), corner_dofs.col(1), corner_dofs.col(2));
		else if(el_type == 4) res.col(i) = bil_trafo(res.col(i), corner_dofs.col(0), corner_dofs.col(1), corner_dofs.col(3), corner_dofs.col(2));
		else std::cout << "\nStrange element encountered.\n";
	}
	
	return res;
}

					
/// Assuming const order el.
nodes_t get_dofs_high_order2(el_order_t el_order, Eigen::Matrix<numeric_t, 2, -1> corner_dofs){
	index_t el_type = corner_dofs.cols();
	index_t num_dofs = get_num_dofs(el_type, el_order);
	nodes_t res(2, num_dofs);
	
	coords_t base_corner = corner_dofs.col(0);
	coords_t d1 = (corner_dofs.col(1) - base_corner) / el_order;
	coords_t d2 = (corner_dofs.col(2) - base_corner) / el_order;
	index_t counter = 0;
	for(index_t i = 0; i < el_order + 1; ++i){
		for(index_t k = 0; k < el_order - (el_type == 4 ? 0 : i) + 1; ++k){
			res.col(counter) = base_corner + k * d1 + i * d2;
			counter ++;
		}
	}
	return res;
}
					

/// More general mesh generation
struct Layer {
	const double offset;
	index_vec_t ids_vec;
	index_t num_inner_els;
	index_t num_outer_els;
	Layer(double offset, index_vec_t ids_vec): offset(offset), ids_vec(ids_vec) {
		num_inner_els = 0;
		num_outer_els = 0;
		for(index_t i = 0; i < ids_vec.size(); ++i){
			if(ids_vec(i) == 3) num_inner_els++;
			else if(ids_vec(i) == 2) num_outer_els++;
			else {
				num_inner_els++;
				num_outer_els++;
			}; 
		};
	};
};

/// Assuming constant order 
void get_num_dofs_layer(index_t & dofs_on_last_layer, index_t & num_new_dofs, Layer l, index_t order, 
							index_t order_next_layer){
	index_t doll_old = dofs_on_last_layer;
	index_t n_el_spec = l.ids_vec.size();
	index_t count_inner = 0;
	index_t count_outer = 0;
	for(index_t i = 0; i < n_el_spec; ++i){
		if(l.ids_vec(i) != 2) count_inner++;
		if(l.ids_vec(i) != 3) count_outer++;
	};
	if(count_outer < count_inner) std::cout << "\nStrange Mesh encountered.\n";
	if(dofs_on_last_layer % count_inner != 0) std::cout << "\nLayers not compatible.\n";
	const index_t multip = dofs_on_last_layer / count_inner;
	dofs_on_last_layer = count_outer * multip;
	num_new_dofs = 0;
	for(unsigned int i = 0; i < n_el_spec; ++i){
		num_new_dofs += l.ids_vec(i) == 1 ? get_num_dofs(4, order - 1) : 
						(l.ids_vec(i) == 2 ? get_num_dofs(3, order - 1) : 
						(order == 1 ? 0 : get_num_dofs(3, order - 2))); 
	}
	num_new_dofs *= multip;
	if(order_next_layer < order) {
		num_new_dofs -= dofs_on_last_layer;
	};
};

/// Works only for constant order elements
nodes_t get_edge_dofs_and_ids(index_vec_t & indxs, const el_order_t el_order, 
							const el_type_t el_type, const nodes_t & el_nodes){
	nodes_t res(2, el_type * (el_order + 1));
	indxs.resize((el_order + 1) * el_type);
	res.leftCols(el_order + 1) = el_nodes.leftCols(el_order + 1);
	for(index_t i = 0; i < el_type * (el_order + 1); ++i) indxs(i) = i;
	index_t count = el_order;
	
	for(index_t i = 0; i < el_order + 1; ++i){
		count = (i + 1) * (el_order + 1) - 1 - (el_type == 3 ? i * (i + 1) / 2: 0);
		res.col(i + el_order  + 1) = el_nodes.col(count);
		indxs(i + el_order + 1) = count;
	}
	for(index_t i = 0; i < el_order + 1; ++i){
		count = i * (el_order + 1) - (el_type == 3 ? i * (i - 1) / 2: 0);
		indxs(i + 2 * (el_order + 1)) = count;
		res.col(i + 2 * (el_order + 1)) = el_nodes.col(count);
	}
	//std::cout << indxs.transpose() << "<-indexes\n";
	if(el_type == 4){
		res.rightCols(el_order + 1) = el_nodes.rightCols(el_order + 1);
	}
	return res;
}
/// Works only for constant order elements
index_vec_t get_edge_dof_indices(const el_order_t el_order, const el_type_t el_type){
	index_vec_t indxs((el_order + 1) * el_type);
	for(index_t i = 0; i < el_type * (el_order + 1); ++i) indxs(i) = i;
	index_t count = el_order;
	
	for(index_t i = 0; i < el_order + 1; ++i){
		count = (i + 1) * (el_order + 1) - 1 - (el_type == 3 ? i * (i + 1) / 2: 0);
		indxs(i + el_order + 1) = count;
	}
	for(index_t i = 0; i < el_order + 1; ++i){
		count = i * (el_order + 1) - (el_type == 3 ? i * (i - 1) / 2: 0);
		indxs(i + 2 * (el_order + 1)) = count;
	}
	//std::cout << indxs.transpose() << "<-indexes\n";
	return indxs;
}

coords_t rot_about_origin(numeric_t dist, numeric_t angle){
	coords_t crd;
	crd(0) = dist * std::cos(2.0 * M_PI * angle);
	crd(1) = dist * std::sin(2.0 * M_PI * angle);
	return crd;
};


/// ids: 1: quad, 2: tria pointing inwards, 3: tria pointing outwards
template<class Vector, class Layers, class Func>
mesh construct_mesh(const Layers & layer_vec, const Vector & orders, const index_t n_tria_in_center, nodes_t & nodes, const Func & r, const bool center_higher_order = false){
	const unsigned int n = orders.size();
	nodes.setZero();
	
	/// Constant width of layers
	numeric_t dr = r(1.0 / (n + 1));
	
	/// Calc orders
	index_vec_t order_desc(n);
	for(index_t i = 0; i < n; ++i){
		order_desc(i) = (i != n-1) && (orders(i + 1) < orders(i));
	}
	
	/// Calculate num_dofs, num_boundary_dofs and more
	index_t num_boundary_dofs;
	index_t num_els = n_tria_in_center;
	index_t order_in_center = orders(0) + center_higher_order;
	index_t num_dofs_in_center = n_tria_in_center * (get_num_dofs(3, order_in_center) - order_in_center - 1) + 1 - center_higher_order * n_tria_in_center;
	index_t num_dofs = num_dofs_in_center;
	index_vec_t num_els_sharing_edge_with_layer(n + 1);
	index_vec_t num_new_dofs_per_layer(n);
	
	num_els_sharing_edge_with_layer(0) = n_tria_in_center;
	index_t els_sharing_edge_with = n_tria_in_center;
	index_t num_new_dofs = 0;
	
	for(int i = 0; i < n - 1; ++i){
		get_num_dofs_layer(els_sharing_edge_with, num_new_dofs, layer_vec[i], orders(i), orders(i + 1)); 
		num_els_sharing_edge_with_layer(i + 1) = els_sharing_edge_with;
		//if(order_desc(i)) num_new_dofs -= els_sharing_edge_with;
		num_new_dofs_per_layer(i) = num_new_dofs;
		num_dofs += num_new_dofs;
	}
	get_num_dofs_layer(els_sharing_edge_with, num_new_dofs, layer_vec[n-1], orders(n-1), orders(n-1)); 
	num_els_sharing_edge_with_layer(n) = els_sharing_edge_with;
	num_dofs += num_new_dofs;
	num_new_dofs_per_layer(n-1) = num_new_dofs;
	//num_boundary_dofs = order_in_center * num_els_sharing_edge_with_layer(0);
	num_boundary_dofs = orders(n-1) * num_els_sharing_edge_with_layer(n);
	std::cout << "\nCreating mesh with: ";
	std::cout << "\nNumber of tria elements in center: " << n_tria_in_center;
	std::cout << "\nNumber of dofs in center: " << num_dofs_in_center;
	std::cout << "\nNumber of dofs in whole mesh: " << num_dofs;
	std::cout << "\nNumber of dofs on the boundary: " << num_boundary_dofs;
	
	//std::cout << "\nnew_dofs_per_layer: " << num_new_dofs_per_layer.transpose() << "\n";
	//std::cout << "\nels_sharing_edge_with_layer: " << num_els_sharing_edge_with_layer.transpose() << "\n";
	
	
	
	/// Create the mesh
	nodes.resize(2, num_dofs);
	element_vec_t elements;
	/// First the interior
	index_t n_dofs_per_tria = get_num_dofs(3, order_in_center);
	coords_t center; center << 0,0;
	coords_t dof = rot_about_origin(dr, 0);
	nodes.col(num_boundary_dofs) = center;	
	coords_t next_corner = dof;
	coords_t curr_corner;
	
	index_vec_t curr_edge_dofs;
	index_vec_t next_edge_dofs;
	
	index_t ind_layer_boundary_dofs = num_boundary_dofs + num_dofs_in_center - order_in_center * n_tria_in_center;
	//std::cout << ind_layer_boundary_dofs << "<-ind_layer_boundary_dofs\n";
	index_t curr_dof_ind = num_boundary_dofs + 1;
	
	/// Center part consistiong of triangles only
	if(true){	
		/////////////////////////////////////////
		numeric_t c_offset = 0.0;
		index_t ct_els = 0;
		
		numeric_t curr_inner_angle = c_offset ;
		numeric_t curr_outer_angle = c_offset;
		
		coords_t curr_element_outer_boundary_coord = rot_about_origin(r(1.0 / (n + 1.)), curr_outer_angle);
		coords_t prev_element_outer_boundary_coord;
		coords_t curr_element_inner_boundary_coord = rot_about_origin(0.0, curr_inner_angle);
		coords_t prev_element_inner_boundary_coord = rot_about_origin(r(0.0), curr_inner_angle);
		numeric_t outer_angle_increment = 1.0 / n_tria_in_center;
		numeric_t inner_angle_increment = 0.0;
		
		
		el_type_t curr_order = order_in_center;
		bool next_layer_smaller_order = center_higher_order; 
		
		index_t inner_layer_boundary_dof_count = curr_dof_ind - 1;
		index_t outer_layer_boundary_dof_count = num_dofs_in_center + num_boundary_dofs - n_tria_in_center * orders(0);
		index_t interior_dof_count = curr_dof_ind;
		
	
		for(index_t i = 0; i < n_tria_in_center; ++i){
			
			bool last_el_of_layer = (i == n_tria_in_center - 1);
			el_type_t el_type = 2;
			el_type_t prev_el_type = 2;
			el_type_t next_el_type = 2;
			
			
			curr_outer_angle += outer_angle_increment;
			prev_element_outer_boundary_coord = curr_element_outer_boundary_coord;
			curr_element_outer_boundary_coord = 
								rot_about_origin(r(1.0/ (n + 1.)), curr_outer_angle);
			
			
			index_vec_t curr_tria_dof_indices(get_num_dofs(3, curr_order));
			curr_tria_dof_indices.setZero();	
					
			nodes_t curr_corners(2, 3); 
			curr_corners << prev_element_outer_boundary_coord, curr_element_outer_boundary_coord,
									curr_element_inner_boundary_coord;
			
			//std::cout << "Current corners: \n" << curr_corners << "\n";
			nodes_t curr_tria_dofs = get_dofs_high_order(curr_order, curr_corners);
			
			index_vec_t loc_boundary_indices;					
			nodes_t curr_edge_dofs = get_edge_dofs_and_ids(loc_boundary_indices, curr_order, 
									3, curr_tria_dofs);
					
			/// New Layer Boundary Dofs
			if(next_layer_smaller_order) {						
				nodes_t smaller_order_nodes = get_dofs_high_order(curr_order - 1, curr_corners);
				for(index_t l = 1; l < curr_order - 1; ++l){
					curr_tria_dofs.col(l) = smaller_order_nodes.col(l);
				}
			}
			for(index_t l = 0; l < curr_order - next_layer_smaller_order; ++l){
				nodes.col(outer_layer_boundary_dof_count) = curr_tria_dofs.col(l);
				curr_tria_dof_indices(l) = outer_layer_boundary_dof_count;
				//std::cout << "\nOLBDC3: " << outer_layer_boundary_dof_count;
				outer_layer_boundary_dof_count++;
			}
					
			/// New Interior Dofs
			index_t d_ct = curr_order + 1;
			for(index_t l = 0; l < curr_order - 1; ++l){
				for(index_t m = 0; m < curr_order - l - 1; ++m){
					nodes.col(interior_dof_count) = curr_tria_dofs.col(d_ct);
					curr_tria_dof_indices(d_ct) = interior_dof_count;
					interior_dof_count++;
					d_ct++;
				}
				d_ct++;						
			}
					
					
			/// Dof indices from last Layer
			curr_tria_dof_indices(get_num_dofs(3, curr_order) - 1) = inner_layer_boundary_dof_count; 
					
			/// Dofs from next element
			index_vec_t last_loc_boundary_indices;
			last_loc_boundary_indices = get_edge_dof_indices(curr_order - 2, 3);
					
			//std::cout << "\nlast_loc_boundary_indices" << last_loc_boundary_indices.transpose() << "\n";
			for(index_t l = 1; l < curr_order; ++l){
				curr_tria_dof_indices(loc_boundary_indices((curr_order + 1) + l)) = 
						interior_dof_count
						- (last_el_of_layer ? num_dofs_in_center - 1 -  
						n_tria_in_center * (curr_order - next_layer_smaller_order):0) + 
						last_loc_boundary_indices((curr_order - (next_el_type == 1 ? 0: 1)) * 2 + l - 1);
			}
			
			curr_tria_dof_indices(curr_order) = (last_el_of_layer ? 
						outer_layer_boundary_dof_count - (curr_order - next_layer_smaller_order) * n_tria_in_center
						: outer_layer_boundary_dof_count);
					
			index_vec_t act_tria_dof_indices(get_num_dofs(3, curr_order) - 1);
			if(next_layer_smaller_order){
				for(index_t l = 0; l < curr_order - 1; ++l){
					act_tria_dof_indices(l) = curr_tria_dof_indices(l);
				}
				for(index_t l = curr_order - 1; l < get_num_dofs(3, curr_order) - 1; ++l){
					act_tria_dof_indices(l) = curr_tria_dof_indices(l + 1);
				}
			}
					
			/// Create the element
			//elements.push_back(element(3, curr_tria_dof_indices, curr_order));
			elements.push_back(element(3, (next_layer_smaller_order ? act_tria_dof_indices : curr_tria_dof_indices), curr_order, next_layer_smaller_order));
			//std::cout << "\n" << act_tria_dof_indices.transpose() << "<-nel_dof_indices\n";
			
		} 		
	}
	
	//std::cout << "\nnew_dofs_per_layer: " << num_new_dofs_per_layer.transpose() << "\n";
	//std::cout << "\nels_sharing_edge_with_layer: " << num_els_sharing_edge_with_layer.transpose() << "\n";
	
	//std::cout << "Inner part of mesh generated.\n";
	
	index_t first_layer_interior_dof = num_dofs_in_center + num_boundary_dofs;
	index_t first_prev_lbd_dof = first_layer_interior_dof - n_tria_in_center * orders(0);
	index_t first_curr_lbd_dof = n > 1 ? first_layer_interior_dof + num_new_dofs_per_layer(0) 
							- num_els_sharing_edge_with_layer(1) * orders(1) : 0;
	
	//std::cout << "\nfirst_prev_lbd_dof: " << first_prev_lbd_dof;
	//std::cout << "\nfirst_curr_lbd_dof: " << first_curr_lbd_dof;
	//std::cout << "\nfirst_layer_interior_dof: " << first_layer_interior_dof;
	//std::cout << "\n";
	
	index_t inner_layer_boundary_dof_count = first_prev_lbd_dof;
	index_t outer_layer_boundary_dof_count = first_curr_lbd_dof;
	index_t interior_dof_count = first_layer_interior_dof;
	
	
	numeric_t cumulated_offset = 0.0;
	/// Then the rest
	for(index_t i = 0; i < n; ++i){
		index_t ct_els = 0;
		
		numeric_t curr_inner_angle = cumulated_offset ;
		cumulated_offset += layer_vec[i].offset / num_els_sharing_edge_with_layer(i + 1);
		numeric_t curr_outer_angle = cumulated_offset;
		
		coords_t curr_element_outer_boundary_coord = rot_about_origin(r((i + 2.0) / (n + 1.)), curr_outer_angle);
		coords_t prev_element_outer_boundary_coord;
		coords_t curr_element_inner_boundary_coord = rot_about_origin(r((i + 1.0) / (n + 1.)), curr_inner_angle);
		coords_t prev_element_inner_boundary_coord;
		numeric_t outer_angle_increment = 1.0 / num_els_sharing_edge_with_layer(i + 1);
		numeric_t inner_angle_increment = 1.0 / num_els_sharing_edge_with_layer(i);
		
		index_t inner_caution_index = 0;
		const index_t layer_block_length = layer_vec[i].ids_vec.size();
		bool inner_bool = true;
		while(inner_caution_index < layer_block_length and inner_bool){
			if(layer_vec[i].ids_vec(layer_block_length - 1 - inner_caution_index) == 2) inner_caution_index++;
			else inner_bool = false;
		}
		index_t outer_caution_index = 0;
		bool outer_bool = true;
		while(outer_caution_index < layer_block_length and outer_bool){
			if(layer_vec[i].ids_vec(layer_block_length - 1 - outer_caution_index) == 3) outer_caution_index++;
			else outer_bool = false;
		}
		
		el_type_t curr_order = orders(i);
		bool next_layer_smaller_order = order_desc(i); 
		
		//std::cout << "\nInner Caution Index: " << inner_caution_index;
		//std::cout << "\nOuter Caution Index: " << outer_caution_index;
		//std::cout << "\nLayer: " << i << "\n";
		//std::cout << "\ninner_layer_boundary_dof_count: " << inner_layer_boundary_dof_count;
		//std::cout << "\nouter_layer_boundary_dof_count: " << outer_layer_boundary_dof_count;
		//std::cout << "\ninterior_dof_count: " << interior_dof_count;
		//std::cout << "\nNext Layer has smaller order? " << next_layer_smaller_order;
		//std::cout << "\n";
		
		index_t n_el_in_layer_part = num_els_sharing_edge_with_layer(i) / layer_vec[i].num_inner_els;
		//std::cout << "\nn_el_in_layer_part: " << n_el_in_layer_part;
		
		for(index_t j = 0; j < n_el_in_layer_part; ++j){
			//std::cout << ct_els << "<-el count\n";
			for(index_t k = 0; k < layer_vec[i].ids_vec.size(); ++k){
				
				bool last_el_of_layer = (j == n_el_in_layer_part-1 && k == layer_vec[i].ids_vec.size() - 1);
				el_type_t el_type = layer_vec[i].ids_vec(k);
				el_type_t prev_el_type = layer_vec[i].ids_vec(k != 0 ? k-1: layer_vec[i].ids_vec.size() - 1);
				el_type_t next_el_type = layer_vec[i].ids_vec(k == layer_vec[i].ids_vec.size() - 1 ? 0: k + 1);
				
				if(el_type != 3) {
					curr_outer_angle += outer_angle_increment;
					prev_element_outer_boundary_coord = curr_element_outer_boundary_coord;
					curr_element_outer_boundary_coord = 
								rot_about_origin(r((i + 2.0) / (n + 1.)), curr_outer_angle);
				}
				if(el_type != 2) {
					curr_inner_angle += inner_angle_increment;
					prev_element_inner_boundary_coord = curr_element_inner_boundary_coord;
					curr_element_inner_boundary_coord = 
								rot_about_origin(r((i + 1.0) / (n + 1.)), curr_inner_angle);
				}					
				
				
				if(el_type == 1) {/// QUAD					
					
					
					index_vec_t curr_quad_dof_indices(get_num_dofs(4, orders(i)));
					curr_quad_dof_indices.setZero();
					
					nodes_t curr_corners(2, 4); 
					curr_corners << prev_element_outer_boundary_coord, curr_element_outer_boundary_coord,
									prev_element_inner_boundary_coord, curr_element_inner_boundary_coord;
					nodes_t curr_quad_dofs = get_dofs_high_order(curr_order, curr_corners);
					
					//std::cout << "\nCorners:\n " << curr_corners << "\n";
					//std::cout << "\ncurr_quad_dofs:\n " << curr_quad_dofs << "\n";
										
					index_vec_t loc_boundary_indices;
					
					nodes_t curr_edge_dofs = get_edge_dofs_and_ids(loc_boundary_indices, curr_order, 
									4, curr_quad_dofs);
					
					/// New Layer Boundary Dofs
					if(next_layer_smaller_order) {						
						nodes_t smaller_order_nodes = get_dofs_high_order(curr_order - 1, curr_corners);
						for(index_t l = 1; l < curr_order - 1; ++l){
							curr_quad_dofs.col(l) = smaller_order_nodes.col(l);
						}
					}
					for(index_t l = 0; l < curr_order - next_layer_smaller_order; ++l){
						nodes.col(outer_layer_boundary_dof_count) = curr_quad_dofs.col(l);
						curr_quad_dof_indices(l) = outer_layer_boundary_dof_count;
						//std::cout << "\nOLBDC4: " << outer_layer_boundary_dof_count;
						outer_layer_boundary_dof_count++;
					}
					
					
					/// New Interior Dofs
					index_t d_ct = curr_order + 1;
					for(index_t l = 0; l < curr_order - 1; ++l){
						for(index_t m = 0; m < curr_order; ++m){
							nodes.col(interior_dof_count) = curr_quad_dofs.col(d_ct);
							curr_quad_dof_indices(d_ct) = interior_dof_count;
							interior_dof_count++;
							//std::cout << "\ndct: " << interior_dof_count;
							d_ct++;
						}
						d_ct++;
					}
					
					/// Dof indices from last Layer
					d_ct = (curr_order + 1) * curr_order;
					for(index_t l = 0; l < curr_order; ++l){
						curr_quad_dof_indices(d_ct) = inner_layer_boundary_dof_count;
						//std::cout << "\ndc: " << d_ct << " ind: " << inner_layer_boundary_dof_count << "\n";
						inner_layer_boundary_dof_count++;
						d_ct++;
					}
					curr_quad_dof_indices(get_num_dofs(4, curr_order) - 1) = inner_layer_boundary_dof_count -
								(last_el_of_layer or  (j == n_el_in_layer_part - 1 && 
								inner_caution_index >= layer_vec[i].ids_vec.size() - k - 1)? 
								curr_order * num_els_sharing_edge_with_layer(i) : 0); 
					
					
					/// Dofs from next element
					index_vec_t last_loc_boundary_indices;
					if(next_el_type == 1) last_loc_boundary_indices = get_edge_dof_indices(curr_order - 1, 4);
					if(next_el_type > 1) last_loc_boundary_indices = get_edge_dof_indices(curr_order - 2, 3);
					
					//std::cout << "\nlast_loc_boundary_indices" << last_loc_boundary_indices.transpose() << "\n";
					for(index_t l = 1; l < curr_order; ++l){
						curr_quad_dof_indices(loc_boundary_indices((curr_order + 1) + l)) = 
								interior_dof_count
								- (last_el_of_layer ? num_new_dofs_per_layer(i) - 
								num_els_sharing_edge_with_layer(i + 1) * (curr_order - next_layer_smaller_order):0) + 
								last_loc_boundary_indices((curr_order - (next_el_type == 1 ? 0: 1)) * 2 + l - 1);
					}
					
					curr_quad_dof_indices(curr_order) = (last_el_of_layer  or  (j == n_el_in_layer_part - 1 && 
								outer_caution_index >= layer_vec[i].ids_vec.size() - k - 1)? 
								(i == n-1 ? 0 : outer_layer_boundary_dof_count - (curr_order- next_layer_smaller_order) * num_els_sharing_edge_with_layer(i+1)
								): outer_layer_boundary_dof_count);
					
					index_vec_t act_quad_dof_indices(get_num_dofs(4, curr_order) - 1);
					if(next_layer_smaller_order){
						for(index_t l = 0; l < curr_order - 1; ++l){
							act_quad_dof_indices(l) = curr_quad_dof_indices(l);
						}
						for(index_t l = curr_order - 1; l < get_num_dofs(4, curr_order) - 1; ++l){
							act_quad_dof_indices(l) = curr_quad_dof_indices(l + 1);
						}
					}
					/// Create the element
					elements.push_back(element(4, (next_layer_smaller_order ? act_quad_dof_indices : curr_quad_dof_indices), curr_order, next_layer_smaller_order));
					//std::cout << "\n" << (next_layer_smaller_order ? act_quad_dof_indices.transpose() : curr_quad_dof_indices.transpose()) << "<-nel_dof_indexes\n";
					//std::cout << "\n" << curr_quad_dof_indices.transpose() << "<-nel_dof_indexes\n";
					
				} else if(el_type == 2) {///TRIA pointing inwards
					
					index_vec_t curr_tria_dof_indices(get_num_dofs(3, orders(i)));
					curr_tria_dof_indices.setZero();	
					
					nodes_t curr_corners(2, 3); 
					curr_corners << prev_element_outer_boundary_coord, curr_element_outer_boundary_coord,
									curr_element_inner_boundary_coord;
					nodes_t curr_tria_dofs = get_dofs_high_order(curr_order, curr_corners);
					
					index_vec_t loc_boundary_indices;					
					nodes_t curr_edge_dofs = get_edge_dofs_and_ids(loc_boundary_indices, curr_order, 
									3, curr_tria_dofs);
					
					
					
					/// New Layer Boundary Dofs
					if(next_layer_smaller_order) {						
						nodes_t smaller_order_nodes = get_dofs_high_order(curr_order - 1, curr_corners);
						for(index_t l = 1; l < curr_order - 1; ++l){
							curr_tria_dofs.col(l) = smaller_order_nodes.col(l);
						}
					}
					for(index_t l = 0; l < curr_order - next_layer_smaller_order; ++l){
						nodes.col(outer_layer_boundary_dof_count) = curr_tria_dofs.col(l);
						curr_tria_dof_indices(l) = outer_layer_boundary_dof_count;
						//std::cout << "\nOLBDC3: " << outer_layer_boundary_dof_count;
						outer_layer_boundary_dof_count++;
					}
					
					/// New Interior Dofs
					index_t d_ct = curr_order + 1;
					for(index_t l = 0; l < curr_order - 1; ++l){
						for(index_t m = 0; m < curr_order - l - 1; ++m){
							nodes.col(interior_dof_count) = curr_tria_dofs.col(d_ct);
							curr_tria_dof_indices(d_ct) = interior_dof_count;
							interior_dof_count++;
							d_ct++;
						}
						d_ct++;						
					}
					
					
					/// Dof indices from last Layer
					curr_tria_dof_indices(get_num_dofs(3, curr_order) - 1) = inner_layer_boundary_dof_count -
								(last_el_of_layer or  (j == n_el_in_layer_part - 1 && 
								inner_caution_index >= layer_vec[i].ids_vec.size() - k - 1)? 
								curr_order * num_els_sharing_edge_with_layer(i) : 0); 
					
					/// Dofs from next element
					index_vec_t last_loc_boundary_indices;
					if(next_el_type == 1) last_loc_boundary_indices = get_edge_dof_indices(curr_order - 1, 4);
					if(next_el_type > 1) last_loc_boundary_indices = get_edge_dof_indices(curr_order - 2, 3);
					
					//std::cout << "\nlast_loc_boundary_indices" << last_loc_boundary_indices.transpose() << "\n";
					for(index_t l = 1; l < curr_order; ++l){
						curr_tria_dof_indices(loc_boundary_indices((curr_order + 1) + l)) = 
								interior_dof_count
								- (last_el_of_layer ? num_new_dofs_per_layer(i) - 
								num_els_sharing_edge_with_layer(i + 1) * (curr_order - next_layer_smaller_order):0) + 
								last_loc_boundary_indices((curr_order - (next_el_type == 1 ? 0: 1)) * 2 + l - 1);
					}
					
					curr_tria_dof_indices(curr_order) = (last_el_of_layer  or  (j == n_el_in_layer_part - 1 && 
								outer_caution_index >= layer_vec[i].ids_vec.size() - k - 1)? 
								(i == n-1 ? 0 : outer_layer_boundary_dof_count - (curr_order - next_layer_smaller_order) * num_els_sharing_edge_with_layer(i+1)
								): outer_layer_boundary_dof_count);
					
					index_vec_t act_tria_dof_indices(get_num_dofs(3, curr_order) - 1);
					if(next_layer_smaller_order){
						for(index_t l = 0; l < curr_order - 1; ++l){
							act_tria_dof_indices(l) = curr_tria_dof_indices(l);
						}
						for(index_t l = curr_order - 1; l < get_num_dofs(3, curr_order) - 1; ++l){
							act_tria_dof_indices(l) = curr_tria_dof_indices(l + 1);
						}
					}
					
					/// Create the element
					//elements.push_back(element(3, curr_tria_dof_indices, curr_order));
					elements.push_back(element(3, (next_layer_smaller_order ? act_tria_dof_indices : curr_tria_dof_indices), curr_order, next_layer_smaller_order));
					//std::cout << "\n" << curr_tria_dof_indices.transpose() << "<-nel_dof_indexes\n";
					
				} else if(el_type == 3) {///TRIA pointing outwards
					
					
					index_vec_t curr_tria_dof_indices(get_num_dofs(3, orders(i)));
					curr_tria_dof_indices.setZero();					
					nodes_t curr_corners(2, 3); 
					curr_corners << curr_element_outer_boundary_coord,
									curr_element_inner_boundary_coord,
									prev_element_inner_boundary_coord;
					nodes_t curr_tria_dofs = get_dofs_high_order(curr_order, curr_corners);
					
					index_vec_t loc_boundary_indices;					
					nodes_t curr_edge_dofs = get_edge_dofs_and_ids(loc_boundary_indices, curr_order, 
									3, curr_tria_dofs);
					
					/// New Interior Dofs
					index_t d_ct = curr_order + 1;
					for(index_t l = 0; l < curr_order - 1; ++l){
						for(index_t m = 0; m < curr_order - l - 1; ++m){
							nodes.col(interior_dof_count) = curr_tria_dofs.col(d_ct);
							curr_tria_dof_indices(d_ct) = interior_dof_count;
							interior_dof_count++;
							//std::cout << "\ndct: " << d_ct;
							d_ct++;
						}
						d_ct++;						
					}
					
					/// Dofs from next element
					index_vec_t last_loc_boundary_indices;
					if(next_el_type == 1) last_loc_boundary_indices = get_edge_dof_indices(curr_order - 1, 4);
					if(next_el_type > 1) last_loc_boundary_indices = get_edge_dof_indices(curr_order - 2, 3);
					
					//std::cout << "\nlast_loc_boundary_indices" << last_loc_boundary_indices.transpose() << "\n";
					for(index_t l = 1; l < curr_order; ++l){
						curr_tria_dof_indices(loc_boundary_indices(l)) = 
								interior_dof_count
								- (last_el_of_layer ? num_new_dofs_per_layer(i) - 
								num_els_sharing_edge_with_layer(i + 1) * (curr_order - next_layer_smaller_order):0) + 
								last_loc_boundary_indices((curr_order - (next_el_type == 1 ? 0: 1)) * 2 + l - 1);
					}
					
					curr_tria_dof_indices(0) = (last_el_of_layer  or  (j == n_el_in_layer_part - 1 && 
								outer_caution_index >= layer_vec[i].ids_vec.size() - k - 1)? 
								(i == n-1 ? 0 : outer_layer_boundary_dof_count - (curr_order - next_layer_smaller_order) * num_els_sharing_edge_with_layer(i+1)
								): outer_layer_boundary_dof_count);
					
					
					/// Dof indices from last Layer
					inner_layer_boundary_dof_count += curr_order;
					for(index_t l = 0; l < curr_order + 1; ++l){
						curr_tria_dof_indices(loc_boundary_indices((curr_order + 1) + l)) = inner_layer_boundary_dof_count - l;
						//std::cout << "\ndc: " << d_ct << " ind: " << inner_layer_boundary_dof_count << "\n";
						//inner_layer_boundary_dof_count++;
					}
					
					curr_tria_dof_indices(curr_order) = inner_layer_boundary_dof_count -
								(last_el_of_layer or  (j == n_el_in_layer_part - 1 && 
								inner_caution_index >= layer_vec[i].ids_vec.size() - k - 1)? 
								curr_order * num_els_sharing_edge_with_layer(i) : 0); 
					
					//interior_dof_count += get_num_dofs(3, curr_order - 2);
					
					/// Create the element
					elements.push_back(element(3, curr_tria_dof_indices, curr_order));
					//std::cout << "\n" << curr_tria_dof_indices.transpose() << "<-nel_dof_indexes\n";
				}
			}			
		}
		//std::cout << "\ninner_layer_boundary_dof_count: " << inner_layer_boundary_dof_count;
		//std::cout << "\nouter_layer_boundary_dof_count: " << outer_layer_boundary_dof_count;
		//std::cout << "\ninterior_dof_count: " << interior_dof_count;
		//std::cout << "\n";
		
		
		if(i != n-1){
			inner_layer_boundary_dof_count = interior_dof_count;
			interior_dof_count = outer_layer_boundary_dof_count;
			outer_layer_boundary_dof_count = interior_dof_count + num_new_dofs_per_layer(i + 1)
							- num_els_sharing_edge_with_layer(i + 2) * (i == n - 2 ? orders(i + 1) : orders(i + 2));
			//std::cout << "\nShare Number : " << num_els_sharing_edge_with_layer(i + 2) << "\n";
			//std::cout << "\nNew Dofs : " << num_new_dofs_per_layer(i + 1) << "\n";
		}
		
		if(i == n-2) outer_layer_boundary_dof_count = 0;
	}
	
	/// Create mesh
	std::cout << "\nMesh with " << elements.size() << " elements created.\n";
	return mesh(elements, num_dofs, num_boundary_dofs);
}

mesh hp_mesh(nodes_t & nodes, const index_t num_layers, const index_t n_tria_in_center = 4){
	
	const numeric_t dr = 1.0 / (num_layers + 1);
	auto r = [&](double x){
		for(index_t i = 1; i <= num_layers; ++i){
			if(x < (i + 0.5) * dr){
				index_t temp = 1 << i;
				return 1.0 * (temp - 1.0) / temp; 
			}
		}
		return 1.0;
	};
	
	std::vector<Layer> layer_vec;
	index_vec_t orders(num_layers);
	index_t ct = 3;
	orders(0) = num_layers;
	index_t inner_ord = num_layers - 1;
	for(index_t i = 1; i < num_layers; ++i){
		index_vec_t lay(ct);
		for(index_t j = 0; j <  ct; ++j){
			lay(j) = (j % 2 ? 1 : 2);
		}
		layer_vec.push_back(Layer(0.0 ,lay));
		orders(i) = inner_ord;
		ct *= 2;
		ct++;
		inner_ord--;
	}
	index_vec_t lay(ct);
	for(index_t j = 0; j < ct; ++j){
		lay(j) = (j % 2 ? 3 : 2);
	}
	layer_vec.push_back(Layer(0.0 ,lay));
	
	std::cout << "\nOrders: " << orders.transpose() << "\n";
	return construct_mesh(layer_vec, orders, n_tria_in_center, nodes, r, true);
}

mesh radial_mesh(nodes_t & nodes, const index_t num_layers, const index_t order = 3, const index_t n_tria_in_center = 6){
	auto r = [](double x){
		return std::sin(M_PI * x / 2);
	};
	
	index_vec_t l1(1);
	l1 << 1;
	
	std::vector<Layer> layer_vec;	
	
	for(index_t i = 0; i < num_layers; ++i){
		layer_vec.push_back(Layer(0.0 ,l1));
	};
	index_vec_t orders(num_layers);
	orders.setConstant(order);
	//~ orders(num_layers-1) = order - 1;
	//~ if(num_layers > 1) orders(num_layers-2) = order - 1;

	return construct_mesh(layer_vec, orders, n_tria_in_center, nodes, r);
}

template <class Function>
mesh triangular_mesh(nodes_t & nodes, const index_t num_layers, const index_t n_tria_in_center = 6,
				const Function & r = [](double x){return std::sin(M_PI * x / 2);}){
	
	index_vec_t desc(num_layers);
	desc.setConstant(1);
	
	
	numeric_t r_old = r(1.0 / (1 + num_layers));
	index_t el_edges_p_c = n_tria_in_center;
	for(index_t i = 1; i < num_layers; ++i){
		if(M_PI * 2 * r((i + 1.0)/(1 + num_layers)) / el_edges_p_c / (r((i + 1.0)/(1 + num_layers)) - r((1.0 * i)/(1 + num_layers))) > 1.2) {
			el_edges_p_c *= 2;
			desc(i - 1) = 2;
		} else desc(i - 1) = 1;
	}
	
	index_vec_t l1(2);
	l1 << 2,3;
	index_vec_t l2(3);
	l2 << 2,3,2;
	
	std::vector<Layer> layer_vec;	
	
	for(index_t i = 0; i < num_layers; ++i){
		if(desc(i) == 1) layer_vec.push_back(Layer(-0.5 ,l1));
		if(desc(i) == 2) layer_vec.push_back(Layer(0.0 ,l2));
	};
	index_vec_t orders(num_layers);
	orders.setConstant(2);
	const index_t max_order = 4;
	for(index_t j = 1; j < max_order; ++j){
		for(index_t i = 0; i < (max_order - j) * num_layers / max_order; ++i){
			orders(i)++;
		}
	}
	
	return construct_mesh(layer_vec, orders, n_tria_in_center, nodes, r);
}

template <class Function>
mesh regular_tria_mesh(nodes_t & nodes, const index_t level, const index_t order = 3, const index_t n_tria_in_center = 6,
				const Function & r = [](double x){return std::sin(M_PI * x / 2);}){
	
	const index_t n_lays = 1 << level;
	
	
	std::vector<Layer> layer_vec;
	layer_vec.reserve(n_lays-1);	
	
	for(index_t i = 1; i < n_lays; ++i){
		index_vec_t l1(2*i+1);
		l1.setConstant(2);
		for(index_t j = 0; j < i; ++j){
			l1(2*j+1) = 3;
		}
		layer_vec.push_back(Layer(0.0,l1));
	};
	
	index_vec_t orders(n_lays-1);
	orders.setConstant(order);
	return construct_mesh(layer_vec, orders, n_tria_in_center, nodes, r);
}


struct HPMeshWrapper {
	const index_t num_layers;
	const index_t n_tria_in_center;
	mesh operator() (nodes_t & nodes, index_t & num_dofs, index_t & num_boundary_dofs) const {
		mesh generated_mesh = hp_mesh(nodes, num_layers, n_tria_in_center);
		num_boundary_dofs = generated_mesh.num_boundary_dofs;
		num_dofs = generated_mesh.num_dofs;
		return generated_mesh;
	}
	HPMeshWrapper(const index_t num_layers, const index_t n_tria_in_center = 4): 
				num_layers(num_layers), n_tria_in_center(n_tria_in_center) {};
};	

struct RadialMeshWrapper {
	const index_t num_layers;
	const index_t order;
	const index_t n_tria_in_center;
	mesh operator() (nodes_t & nodes, index_t & num_dofs, index_t & num_boundary_dofs) const {
		mesh generated_mesh = radial_mesh(nodes, num_layers, order, n_tria_in_center);
		num_boundary_dofs = generated_mesh.num_boundary_dofs;
		num_dofs = generated_mesh.num_dofs;
		return generated_mesh;
	}
	RadialMeshWrapper(const index_t num_layers, const index_t n_tria_in_center = 6, const index_t order = 3): 
				num_layers(num_layers), n_tria_in_center(n_tria_in_center), order(order) {};
};	


struct TriangularMeshWrapper {
	const index_t num_layers;
	const index_t n_tria_in_center;
	mesh operator() (nodes_t & nodes, index_t & num_dofs, index_t & num_boundary_dofs) const {
		mesh generated_mesh = triangular_mesh(nodes, num_layers, n_tria_in_center, [](double x){return std::sin(M_PI * x / 2);});
		num_boundary_dofs = generated_mesh.num_boundary_dofs;
		num_dofs = generated_mesh.num_dofs;
		return generated_mesh;
	}
	TriangularMeshWrapper(const index_t num_layers, const index_t n_tria_in_center = 6): 
				num_layers(num_layers), n_tria_in_center(n_tria_in_center) {};
};	

struct RegTriaMeshWrapper {
	const index_t level;
	const index_t order;
	const index_t n_tria_in_center;
	mesh operator() (nodes_t & nodes, index_t & num_dofs, index_t & num_boundary_dofs) const {
		mesh generated_mesh = regular_tria_mesh(nodes, level, order, n_tria_in_center, [](double x){return x;});
		num_boundary_dofs = generated_mesh.num_boundary_dofs;
		num_dofs = generated_mesh.num_dofs;
		return generated_mesh;
	}
	RegTriaMeshWrapper(const index_t level, const index_t n_tria_in_center = 6, const index_t order = 3): 
				level(level), n_tria_in_center(n_tria_in_center), order(order) {};
};	
			
mesh first_try(nodes_t & nodes){
	const index_t n_tria_in_center = 7;
	
	index_vec_t l1(1);
	l1 << 1;
	index_vec_t l2(3);
	l2 << 1,2,1;
	index_vec_t l3(5);
	l3 << 2,1,3,1,2;
	index_vec_t l4(4);
	l4 << 2,2,1,3;
	index_vec_t l5(3);
	l5 << 2,3,2;
	index_vec_t l6(2);
	l6 << 2,3;
	index_vec_t l7(2);
	l7 << 2,1;
	index_vec_t l8(7);
	l8 << 2,3,2,3,2,3,2;
	index_vec_t l9(3);
	l9 << 2,1,2;
	
	
	auto r = [](double x){
		return std::sin(M_PI * x / 2);
	};
	
	std::vector<Layer> layer_vec;	
	
	layer_vec.push_back(Layer(0.5 ,l9));
	//layer_vec.push_back(Layer(-0.5 ,l6));
	layer_vec.push_back(Layer(0.0 ,l8));
	//layer_vec.push_back(Layer(0.0 ,l7));
	//layer_vec.push_back(Layer(-0.5 ,l6));
	//layer_vec.push_back(Layer(0.0 ,l1));
	//layer_vec.push_back(Layer(-0.5 ,l6));
	//layer_vec.push_back(Layer(0.0 ,l5));
	//layer_vec.push_back(Layer(0.0 ,l1));
	//layer_vec.push_back(Layer(-0.5 ,l6));
	
	int N = layer_vec.size();
	index_vec_t orders(N);
	orders.setConstant(1);
	orders *= 1;
	orders(N - 1) = 1;
	//orders(N - 2) = 2;

	return construct_mesh(layer_vec, orders, n_tria_in_center, nodes, r);
}












