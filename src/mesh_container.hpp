
#include <Eigen/Dense>
#include <Eigen/SparseLU>
#include <iostream>

typedef Eigen::Matrix<numeric_t, 2, -1> nodes_t;

typedef unsigned short el_type_t;
typedef unsigned short el_order_t;

struct element {
	const el_type_t el_type;
	const el_order_t order;
	const el_order_t num_missing_dofs;
	index_vec_t nodes;
	element(const el_type_t elt, const index_vec_t nds, const unsigned int order = 1,
				el_order_t num_missing_dofs = 0): 
				el_type(elt), nodes(nds), order(order), num_missing_dofs(num_missing_dofs) {
		// TODO: assertion
		};
	// Only suited for pw linear triangles
	numeric_t area(const nodes_t & nds) const {
		if(el_type == 3){
			const coords_t n1 = nds.col(nodes(0));
			const coords_t n2 = nds.col(nodes(order - (num_missing_dofs > 0)));
			const coords_t n3 = nds.col(nodes(2*order));
			return 0.5*((n2(0)-n1(0))*(n3(1)-n2(1))-(n3(0)-n2(0))*(n2(1)-n1(1)));
		}
		else if(el_type == 4){
			std::cout << "Not implemented yet.\n";
			return 0.;
		}
		std::cout << "Strange element encountered.\n";
		return 0.;
	};
	index_t corner(index_t i) const {
		if(el_type == 3) {
			return (i < 2 ? i * order : (order + 1) * (order + 2) / 2 - 1) - (i > 0 ? num_missing_dofs : 0);
		}
		else if(el_type == 4){
			return (i < 2 ? i * order : (order + 1) * (order + 1) - 1 - (i == 3 ? order : 0)) - (i > 0 ? num_missing_dofs : 0);
		}
		else std::cout << "Strange element encountered.\n";
	};
};

typedef std::vector<element> element_vec_t;

struct mesh {
	element_vec_t elements;
	const index_t numEls;
	const index_t num_dofs;
	const index_t num_boundary_dofs;
	mesh(const element_vec_t elements, const index_t num_dofs, const index_t num_boundary_dofs): 
				elements(elements), num_dofs(num_dofs), num_boundary_dofs(num_boundary_dofs),
				numEls(elements.size()){};
};

el_order_t get_num_dofs(el_type_t el_type, el_order_t el_order){
	return el_type == 3 ? (el_order + 1) * (el_order + 2) / 2: (el_order + 1) * (el_order + 1);
}
	
template <class Function>
void add_to_meshes(std::vector<mesh> & meshes, std::vector<nodes_t> & nodes_vec, 
					std::vector<index_t> & num_dofs_vec, std::vector<index_t> & num_boundary_dofs_vec,
					const Function & mesh_generator){
	const index_t n = meshes.size();
	meshes.push_back(mesh_generator(nodes_vec[n], num_dofs_vec[n], num_boundary_dofs_vec[n]));
};
