#include "shape_functions_tria.hpp"
#include "shape_functions_quad.hpp"
#include "very_high_order_regular_shapefunctions.hpp"

typedef Eigen::Matrix<numeric_t, -1, 2>(*GradBasisFunctionPointer)(const coords_t&);
const index_t MAX_ORD = 12;

const GradBasisFunctionPointer TriaGradients[] = {
					gradb1_tria, 
					gradb2_1_tria, gradb2_tria,
					gradb3_1_tria, gradb3_tria,
					gradb4_1_tria, gradb4_tria,
					gradb5_1_tria, gradb5_tria,
					gradb6_1_tria, gradb6_tria,
					grad_bi_1_tria<7>, grad_bi_tria<7>,
					grad_bi_1_tria<8>, grad_bi_tria<8>,
					grad_bi_1_tria<9>, grad_bi_tria<9>,
					grad_bi_1_tria<10>, grad_bi_tria<10>,
					grad_bi_1_tria<11>, grad_bi_tria<11>,
					grad_bi_1_tria<12>, grad_bi_tria<12>,
					};

const GradBasisFunctionPointer QuadGradients[] = {
					gradb1_quad, 
					gradb2_1_quad, gradb2_quad,
					gradb3_1_quad, gradb3_quad,
					gradb4_1_quad, gradb4_quad,
					gradb5_1_quad, gradb5_quad,
					gradb6_1_quad, gradb6_quad,
					grad_bi_1_quad<7>, grad_bi_quad<7>,
					grad_bi_1_quad<8>, grad_bi_quad<8>,
					grad_bi_1_quad<9>, grad_bi_quad<9>,
					grad_bi_1_quad<10>, grad_bi_quad<10>,
					grad_bi_1_quad<11>, grad_bi_quad<11>,
					grad_bi_1_quad<12>, grad_bi_quad<12>,
					};


typedef Eigen::Matrix<numeric_t, -1, 1>(*BasisFunctionPointer)(const coords_t&);

const BasisFunctionPointer TriaBFs[] = {
					b1_tria, 
					b2_1_tria, b2_tria,
					b3_1_tria, b3_tria,
					b4_1_tria, b4_tria,
					b5_1_tria, b5_tria,
					bi_1_tria<6>, bi_tria<6>,
					bi_1_tria<7>, bi_tria<7>,
					bi_1_tria<8>, bi_tria<8>,
					bi_1_tria<9>, bi_tria<9>,
					bi_1_tria<10>, bi_tria<10>,
					bi_1_tria<11>, bi_tria<11>,
					bi_1_tria<12>, bi_tria<12>,
					};

const BasisFunctionPointer QuadBFs[] = {
					b1_quad, 
					b2_1_quad, b2_quad,
					b3_1_quad, b3_quad,
					b4_1_quad, b4_quad,
					b5_1_quad, b5_quad,
					bi_1_quad<6>, bi_quad<6>,
					bi_1_quad<7>, bi_quad<7>,
					bi_1_quad<8>, bi_quad<8>,
					bi_1_quad<9>, bi_quad<9>,
					bi_1_quad<10>, bi_quad<10>,
					bi_1_quad<11>, bi_quad<11>,
					bi_1_quad<12>, bi_quad<12>,
					};
