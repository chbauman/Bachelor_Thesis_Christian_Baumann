

// Transformation of triangles
auto aff_trafo = [](coords_t x, coords_t a1, coords_t a2, coords_t a3){
	coord_mat_t F;
	F << a2-a1, a3-a1;
	coords_t res = F * x + a1;
	return res;
};
auto D_aff_trafo = [](coords_t a1, coords_t a2, coords_t a3){
	coord_mat_t F;
	F << a2-a1, a3-a1;
	return F;
};
auto detD_aff_trafo_fun = [](coords_t a1, coords_t a2, coords_t a3){
	coord_mat_t F;
	F << a2-a1, a3-a1;
	return std::abs(F(0,0)*F(1,1)-F(1,0)*F(0,1));
};

// Transformation of quadrilaterals
auto bil_trafo = [](coords_t x, coords_t a1, coords_t a2, coords_t a3, coords_t a4){
	coords_t res;
	const coords_t beta = a2 - a1;
	const coords_t gam = a4 - a1;
	const coords_t del = a3-a2-gam;
	res = a1 + x(0) * beta + x(1) * gam + x(0) * x(1) * del;
	return res;
};

auto D_bil_trafo = [](coords_t x, coords_t a1, coords_t a2, coords_t a3, coords_t a4){
	coord_mat_t D;
	const coords_t beta = a2 - a1;
	const coords_t gam = a4 - a1;
	const coords_t del = a3-a2-gam;
	D.col(0) = beta + del * x(1);
	D.col(1) = gam + del * x(0);
	return D;
};

auto detD_bil_trafo_fun = [](coords_t x, coords_t a1, coords_t a2, coords_t a3, coords_t a4){
	const coords_t beta = a2 - a1;
	const coords_t gam = a4 - a1;
	const coords_t del = a3-a2-gam;
	return beta(0)*gam(1) - beta(1)*gam(0) + (beta(0)*del(1) - beta(1)*del(0)) * x(0)
					+ (del(0)*gam(1) - del(1)*gam(0)) * x(1);
};

// Assembly of element matrix of quadrilateral element with shape functions of polynomial
// order "order" (up to 3) and quadrature order "quad_order".
template <unsigned int order, unsigned int quad_order, unsigned int num_missing_dofs, el_type_t el_type, 
				class NODES, class ELEMENT>
lclMat_t assembleLocElMat_straight(const NODES & nds, const ELEMENT & el){
	
	const index_t n_loc_dofs = (el_type == 4 ? (order+1)*(order+1) : (order+1)*(order+2)/2) - num_missing_dofs;
	
	const quadRule QR = (el_type == 4 ? quad_qr(quad_order) : tria_qr(quad_order));
	const quad_weights_t weights = QR.weights;
	const quad_points_t points = QR.points;
	const index_t qr_num_points = QR.n;
	
	Eigen::Matrix<numeric_t, -1, n_loc_dofs> Grads(2*qr_num_points, n_loc_dofs);
	
	const coords_t c1 = nds.col(el.nodes(0));
	const coords_t c2 = nds.col(el.nodes(order - (num_missing_dofs > 0)));
	const coords_t c3 = nds.col(el.nodes(n_loc_dofs - 1));
	
	Eigen::Matrix<numeric_t, -1, 1> detD_bil_at_qp(qr_num_points, 1);
	numeric_t detD_aff_trafo;
	
	if(el_type == 4){
		const coords_t c4 = nds.col(el.nodes(n_loc_dofs-1-order));	
		
		for(index_t i = 0; i < qr_num_points; ++i){
			numeric_t detD_bil_trafo = detD_bil_trafo_fun(points.row(i), c1, c2, c3, c4);
			detD_bil_at_qp(i) = detD_bil_trafo;
			coord_mat_t DPhiinvtrp = D_bil_trafo(points.row(i), c1, c2, c3, c4).inverse().transpose();		
			Grads.template block<2, n_loc_dofs> (2*i, 0) = DPhiinvtrp * 
					QuadGradients[2*(order-1) - num_missing_dofs](points.row(i)).transpose();
		}
	} else if(el_type == 3){		
		const coord_mat_t DPhiinvtrp = D_aff_trafo(c1, c2, c3).inverse().transpose();
		detD_aff_trafo = detD_aff_trafo_fun(c1, c2, c3);

		for(index_t i = 0; i < qr_num_points; ++i){	
			Grads.template block<2, n_loc_dofs> (2*i, 0) = DPhiinvtrp * 
					TriaGradients[2*(order-1) - num_missing_dofs](points.row(i)).transpose();
		}
	}
	
	lclMat_t lclMat(n_loc_dofs, n_loc_dofs);
	lclMat.setZero();
	for(index_t i = 0; i < n_loc_dofs; ++i){
		for(index_t j = 0; j < n_loc_dofs; ++j){
			numeric_t res = 0.0;
			for(index_t k = 0; k < qr_num_points; ++k){
				res += Grads.template block<2, 1>(k*2, i).dot(
							Grads.template block<2, 1>(k*2, j)) * (el_type == 4 ? weights(k) * detD_bil_at_qp(k) : weights(k));
			}
			lclMat(i,j) += (el_type == 4 ? res : res * detD_aff_trafo);
		}
	}
	return lclMat;
};


