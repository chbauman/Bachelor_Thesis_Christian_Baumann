/// Assuming const order el.
template<index_t el_order, index_t el_type>
nodes_t get_ref_dofs(){
	index_t num_dofs = get_num_dofs(el_type, el_order);
	nodes_t res(2, num_dofs);
	
	coords_t base_corner; base_corner << 0.,0.;
	coords_t d1; d1 << 1.,0.; d1 /= el_order;
	coords_t d2; d2 << 0.,1.; d2 /= el_order;
	
	index_t counter = 0;
	for(index_t i = 0; i < el_order + 1; ++i){
		for(index_t k = 0; k < el_order - (el_type == 4 ? 0 : i) + 1; ++k){
			res.col(counter) = base_corner + k * d1 + i * d2;
			counter ++;
		}
	}	
	return res;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Quadrilateral element stuff ////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template<index_t order>
Eigen::Matrix<numeric_t, -1, 1> bi_quad(const coords_t & x){
	
	const index_t n_dofs = (order + 1) * (order + 1);
	
	Eigen::Matrix<numeric_t, -1, 1> res2(n_dofs); res2.setConstant(1.0);
	
	Eigen::Matrix<numeric_t, order + 1, 1> x_facs;
	Eigen::Matrix<numeric_t, order + 1, 1> y_facs;
	
	Eigen::Matrix<numeric_t, order, 1> left_x_facs;
	Eigen::Matrix<numeric_t, order, 1> bottom_y_facs;
	Eigen::Matrix<numeric_t, order, 1> right_x_facs;
	Eigen::Matrix<numeric_t, order, 1> top_y_facs;
	
	for(index_t i = 0; i < order + 1; ++i){
		x_facs(i) = x(0) - i * 1.0 / order;
		y_facs(i) = x(1) - i * 1.0 / order;
	}
	
	left_x_facs(0) = x_facs(0);
	right_x_facs(0) = x_facs(order);
	bottom_y_facs(0) = y_facs(0);
	top_y_facs(0) = y_facs(order);
	
	for(index_t i = 0; i < order - 1; ++i){
		left_x_facs(i + 1) = left_x_facs(i) * x_facs(i + 1);
		right_x_facs(i + 1) = right_x_facs(i) * x_facs(order - i - 1);
		bottom_y_facs(i + 1) = bottom_y_facs(i) * y_facs(i + 1);
		top_y_facs(i + 1) = top_y_facs(i) * y_facs(order - i - 1);
	}
	
	index_t ct = 0; 
	for(index_t i = 0; i < order + 1; ++i){
		for(index_t j = 0; j < order + 1; ++j){
			if(j > 0) res2(ct) *= left_x_facs(j - 1);
			if(i > 0) res2(ct) *= bottom_y_facs(i - 1);
			if(j < order) res2(ct) *= right_x_facs(order - 1 - j);
			if(i < order) res2(ct) *= top_y_facs(order - 1 - i);
			ct++;
		}
	}
	
	/// Normalize
	Eigen::Matrix<numeric_t, order+1,1> x_norm_facs; 
	x_norm_facs.setConstant(1.0);	
	for(int i = 0; i < order + 1; ++i){
		for(int j = 0; j < order + 1; ++j){
			if(i - j) {
				x_norm_facs(i) /= (i - j) * 1.0 / order;
			}
		}
	}	
	for(index_t i = 0; i < n_dofs; ++i){
		res2(i) *= x_norm_facs(i % (order + 1));
		res2(i) *= x_norm_facs(i / (order + 1));
	}
	return res2;
}


template<index_t order>
Eigen::Matrix<numeric_t, -1, 2> grad_bi_quad(const coords_t & x){
	
	const index_t n_dofs = (order + 1) * (order + 1);
	Eigen::Matrix<numeric_t, -1, 2> res(n_dofs,2); res.setConstant(1.0);
	Eigen::Matrix<numeric_t, order + 1, 1> x_facs;
	Eigen::Matrix<numeric_t, order + 1, 1> y_facs;
	
	for(index_t i = 0; i < order + 1; ++i){
		x_facs(i) = x(0) - i * 1.0 / order;
		y_facs(i) = x(1) - i * 1.0 / order;
	}
	
	for(index_t i = 0; i < n_dofs; ++i){
		for(index_t j = 0; j < order + 1; ++j){
			if(j != i / (order + 1)) res(i,0) *= y_facs(j);
			if(j != i % (order + 1)) res(i,1) *= x_facs(j);
		}
	}
	
	for(index_t i = 0; i < n_dofs; ++i){
		numeric_t x_dev_fac = 0;
		numeric_t y_dev_fac = 0;
		for(index_t j = 0; j < order + 1; ++j){
			numeric_t x_temp = 1;
			numeric_t y_temp = 1;
			for(index_t k = 0; k < order + 1; ++k){
				if(k != j && k != i % (order + 1)) x_temp *= x_facs(k);
				if(k != j && k != i / (order + 1)) y_temp *= y_facs(k);
			}
			if(j != i % (order + 1)){
				x_dev_fac += x_temp;
			}
			if(j != i / (order + 1)){
				y_dev_fac += y_temp;
			}
		}
		res(i,0) *= x_dev_fac;
		res(i,1) *= y_dev_fac;
	}
	
	/// Normalize
	Eigen::Matrix<numeric_t, order+1,1> x_norm_facs; 
	x_norm_facs.setConstant(1.0);	
	for(int i = 0; i < order + 1; ++i){
		for(int j = 0; j < order + 1; ++j){
			if(i - j) {
				x_norm_facs(i) /= (i - j) * 1.0 / order;
			}
		}
	}
	
	for(index_t i = 0; i < n_dofs; ++i){
		res.row(i) *= x_norm_facs(i % (order + 1));
		res.row(i) *= x_norm_facs(i / (order + 1));
	}
	return res;
}


template<index_t order>
numeric_t non_normal_quad_bf(const index_t & i, const index_t & j, const coords_t & x){
	
	const index_t n_dofs = (order + 1) * (order + 1);
	
	Eigen::Matrix<numeric_t, order + 1, 1> x_facs;
	Eigen::Matrix<numeric_t, order + 1, 1> y_facs;
	
	Eigen::Matrix<numeric_t, order, 1> left_x_facs;
	Eigen::Matrix<numeric_t, order, 1> bottom_y_facs;
	Eigen::Matrix<numeric_t, order, 1> right_x_facs;
	Eigen::Matrix<numeric_t, order, 1> top_y_facs;
	
	for(index_t i = 0; i < order + 1; ++i){
		x_facs(i) = x(0) - i * 1.0 / order;
		y_facs(i) = x(1) - i * 1.0 / order;
	}
	
	left_x_facs(0) = x_facs(0);
	right_x_facs(0) = x_facs(order);
	bottom_y_facs(0) = y_facs(0);
	top_y_facs(0) = y_facs(order);
	
	for(index_t i = 0; i < order - 1; ++i){
		left_x_facs(i + 1) = left_x_facs(i) * x_facs(i + 1);
		right_x_facs(i + 1) = right_x_facs(i) * x_facs(order - i - 1);
		bottom_y_facs(i + 1) = bottom_y_facs(i) * y_facs(i + 1);
		top_y_facs(i + 1) = top_y_facs(i) * y_facs(order - i - 1);
	}
	
	numeric_t res = 1.0;
	
	if(j > 0) res *= left_x_facs(j - 1);
	if(i > 0) res *= bottom_y_facs(i - 1);
	if(j < order) res *= right_x_facs(order - 1 - j);
	if(i < order) res *= top_y_facs(order - 1 - i);
	
	return res;
}

template<index_t order>
Eigen::Matrix<numeric_t, -1, 1> bi_1_quad(const coords_t & x){
	
	const Eigen::Matrix<numeric_t, -1, 1> sf_high = bi_quad<order>(x);
	const Eigen::Matrix<numeric_t, -1, 1> sf_low = bi_quad<order - 1>(x);
	const index_t n_dofs = (order + 1) * (order + 1) - 1;
	
	Eigen::Matrix<numeric_t, -1, 1> res(n_dofs); 
	res.segment<n_dofs - order>(order) = sf_high.segment<n_dofs - order>(order + 1);
	res.head<order>() = sf_low.head<order>();
	
	const nodes_t dofs_high = get_ref_dofs<order, 4>();
	const nodes_t dofs_low = get_ref_dofs<order - 1, 4>();
	
	const index_t n_corr_fs = n_dofs - order - 1;
	
	for(index_t k = 0; k < order; ++k){
		numeric_t norm_fac = non_normal_quad_bf<order - 1>(0, k, dofs_low.col(k));
		index_t ct = 0;
		for(index_t i = 1; i < order; ++i){
			for(index_t j = 0; j < order + 1; ++j){
				const numeric_t temp = non_normal_quad_bf<order - 1>(0, k, dofs_high.col(ct + order + 1)) / norm_fac;
				res(k) -= temp * res(ct + order);
				ct++;
			}
		}
	}
	return res;
}

template<index_t order>
Eigen::Matrix<numeric_t, -1, 2> grad_bi_1_quad(const coords_t & x){
	
	const Eigen::Matrix<numeric_t, -1, 2> sf_high = grad_bi_quad<order>(x);
	const Eigen::Matrix<numeric_t, -1, 2> sf_low = grad_bi_quad<order - 1>(x);
	const index_t n_dofs = (order + 1) * (order + 1) - 1;
	
	Eigen::Matrix<numeric_t, -1, 2> res(n_dofs, 2); 
	res.middleRows<n_dofs - order>(order) = sf_high.middleRows<n_dofs - order>(order + 1);
	res.topRows<order>() = sf_low.topRows<order>();
	
	const nodes_t dofs_high = get_ref_dofs<order, 4>();
	const nodes_t dofs_low = get_ref_dofs<order - 1, 4>();
	
	const index_t n_corr_fs = n_dofs - order - 1;
	
	for(index_t k = 0; k < order; ++k){
		numeric_t norm_fac = non_normal_quad_bf<order - 1>(0, k, dofs_low.col(k));
		index_t ct = 0;
		for(index_t i = 1; i < order; ++i){
			for(index_t j = 0; j < order + 1; ++j){
				const numeric_t temp = non_normal_quad_bf<order - 1>(0, k, dofs_high.col(ct + order + 1)) / norm_fac;
				res.row(k) -= temp * res.row(ct + order);
				ct++;
			}
		}
	}
	return res;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Triagular element stuff ////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


template<index_t order>
numeric_t non_normal_tria_bf(const index_t & i, const index_t & j, const coords_t & x){
	Eigen::Matrix<numeric_t, order, 1> x_facs;
	Eigen::Matrix<numeric_t, order, 1> y_facs;
	Eigen::Matrix<numeric_t, order, 1> xy_facs;
	
	for(index_t i = 0; i < order; ++i){
		x_facs(i) = x(0) - i * 1.0 / order;
		y_facs(i) = x(1) - i * 1.0 / order;
		xy_facs(i) = x(1) + x(0) - (order - i) * 1.0 / order;
	}
	
	for(index_t i = 1; i < order; ++i){
		x_facs(i) *= x_facs(i - 1);
		y_facs(i) *= y_facs(i - 1);
		xy_facs(i) *= xy_facs(i - 1);
	}
	
	numeric_t temp = 1.0;
	if(i > 0) temp *= y_facs(i - 1);
	if(j > 0) temp *= x_facs(j - 1);
	if(j != order - i) temp *= xy_facs(order - 1 - j - i);
	
	return temp;
}


/// Arbitrary order Shape functions for the triangular reference element
template<index_t order>
Eigen::Matrix<numeric_t, -1, 1> bi_tria(const coords_t & x){
	
	const index_t n_dofs = (order + 1) * (order + 2) / 2;
	
	Eigen::Matrix<numeric_t, -1, 1> res(n_dofs); 
	res.setConstant(1.0);
	Eigen::Matrix<numeric_t, order, 1> x_facs;
	Eigen::Matrix<numeric_t, order, 1> y_facs;
	Eigen::Matrix<numeric_t, order, 1> xy_facs;
	
	for(index_t i = 0; i < order; ++i){
		x_facs(i) = x(0) - i * 1.0 / order;
		y_facs(i) = x(1) - i * 1.0 / order;
		xy_facs(i) = x(1) + x(0) - (order - i) * 1.0 / order;
	}
	
	for(index_t i = 1; i < order; ++i){
		x_facs(i) *= x_facs(i - 1);
		y_facs(i) *= y_facs(i - 1);
		xy_facs(i) *= xy_facs(i - 1);
	}
	
	index_t ct = 0;
	for(index_t i = 0; i < order + 1; ++i){
		for(index_t j = 0; j < order + 1 - i; ++j){
			if(i > 0) res(ct) *= y_facs(i - 1);
			if(j > 0) res(ct) *= x_facs(j - 1);
			if(j != order - i) res(ct) *= xy_facs(order - 1 - j - i);
			ct++;
		}
	}
		
	/// Normalize
	index_t non_cst_order = order;
	Eigen::Matrix<numeric_t, 2, -1> ref_dofs = get_ref_dofs<order,3>();
	
	ct = 0;
	for(index_t i = 0; i < order + 1; ++i){
		for(index_t j = 0; j < order + 1 - i; ++j){
			res(ct) /= non_normal_tria_bf<order>(i, j, ref_dofs.col(ct));
			ct++;
		}
	}
	
	return res;
}

/// And the coresponding gradients
template<index_t order>
Eigen::Matrix<numeric_t, -1, 2> grad_bi_tria(const coords_t & x){
		
	const index_t n_dofs = (order + 1) * (order + 2) / 2;
	
	Eigen::Matrix<numeric_t, -1, 2> res(n_dofs, 2); 
	res.setConstant(1.0);
	Eigen::Matrix<numeric_t, order, 1> x_facs;
	Eigen::Matrix<numeric_t, order, 1> y_facs;
	Eigen::Matrix<numeric_t, order, 1> xy_facs;
	
	for(index_t i = 0; i < order; ++i){
		x_facs(i) = x(0) - i * 1.0 / order;
		y_facs(i) = x(1) - i * 1.0 / order;
		xy_facs(i) = x(1) + x(0) - (order - i) * 1.0 / order;
	}	
	
	Eigen::Matrix<numeric_t, order, 1> x_facs2 = x_facs;
	Eigen::Matrix<numeric_t, order, 1> y_facs2 = y_facs;
	Eigen::Matrix<numeric_t, order, 1> xy_facs2 = xy_facs;
	
	for(index_t i = 1; i < order; ++i){
		x_facs(i) *= x_facs(i - 1);
		y_facs(i) *= y_facs(i - 1);
		xy_facs(i) *= xy_facs(i - 1);
	}
	
	Eigen::Matrix<numeric_t, order, 1> xdx_facs;
	Eigen::Matrix<numeric_t, order, 1> ydy_facs;
	Eigen::Matrix<numeric_t, order, 1> xydx_facs;
	Eigen::Matrix<numeric_t, order, 1> xydy_facs;
	
	xdx_facs(0) = ydy_facs(0) = xydx_facs(0) = xydy_facs(0) = 1.0;
	for(index_t i = 1; i < order; ++i){
		/// Create derivatives...
		xdx_facs(i) = x_facs(i-1) + xdx_facs(i-1)*x_facs2(i);
		ydy_facs(i) = y_facs(i-1) + ydy_facs(i-1)*y_facs2(i);
		xydx_facs(i) = xy_facs(i-1) + xydx_facs(i-1)*xy_facs2(i);
		xydy_facs(i) = xy_facs(i-1) + xydy_facs(i-1)*xy_facs2(i);
	}
	
	
	index_t ct = 0;
	for(index_t i = 0; i < order + 1; ++i){
		for(index_t j = 0; j < order + 1 - i; ++j){
			
			/// factors not affected by derivatives
			if(i > 0) res(ct, 0) *= y_facs(i - 1);//f(y) != 1
			if(j > 0) res(ct, 1) *= x_facs(j - 1);//g(x) != 1
			
			/// remaining factors
			if(j != order - i) {//h(x,y) != 1
				if(i > 0) {
					res(ct, 1) *= (y_facs(i - 1) * xydy_facs(order - 1 - j - i) + ydy_facs(i - 1) * xy_facs(order - 1 - j - i));
				} else {
					res(ct, 1) *= xydy_facs(order - 1 - j - i);
				}
				if(j > 0) {
					res(ct, 0) *= (x_facs(j - 1) * xydx_facs(order - 1 - j - i) + xdx_facs(j - 1) * xy_facs(order - 1 - j - i));
				} else {
					res(ct, 0) *= xydx_facs(order - 1 - j - i);
				}
			} else {
				if(i > 0) res(ct, 1) *= ydy_facs(i - 1);
				else res(ct, 1) = 0.0;
				if(j > 0) res(ct, 0) *= xdx_facs(j - 1);
				else res(ct, 0) = 0.0;
			}
			ct++;
		}
	}
	//std::cout << res << "<-not norm res\n";
	
	/// Normalize
	Eigen::Matrix<numeric_t, 2, -1> ref_dofs = get_ref_dofs<order,3>();
	
	ct = 0;
	for(index_t i = 0; i < order + 1; ++i){
		for(index_t j = 0; j < order + 1 - i; ++j){
			res.row(ct) /= non_normal_tria_bf<order>(i, j, ref_dofs.col(ct));
			ct++;
		}
	}
	
	return res;	
}


template<index_t order>
Eigen::Matrix<numeric_t, -1, 1> bi_1_tria(const coords_t & x){
	
	const Eigen::Matrix<numeric_t, -1, 1> sf_high = bi_tria<order>(x);
	const Eigen::Matrix<numeric_t, -1, 1> sf_low = bi_tria<order - 1>(x);
	const index_t n_dofs = (order + 1) * (order + 2) / 2 - 1;
	
	Eigen::Matrix<numeric_t, -1, 1> res(n_dofs); 
	res.segment<n_dofs - order>(order) = sf_high.segment<n_dofs - order>(order + 1);
	res.head<order>() = sf_low.head<order>();
	
	const nodes_t dofs_high = get_ref_dofs<order, 3>();
	const nodes_t dofs_low = get_ref_dofs<order - 1, 3>();
	
	const index_t n_corr_fs = n_dofs - order - 1;
	
	for(index_t k = 0; k < order; ++k){
		numeric_t norm_fac = non_normal_tria_bf<order - 1>(0, k, dofs_low.col(k));
		index_t ct = 0;
		for(index_t i = 1; i < order; ++i){
			for(index_t j = 0; j < order + 1 - i; ++j){
				const numeric_t temp = non_normal_tria_bf<order - 1>(0, k, dofs_high.col(ct + order + 1)) / norm_fac;
				res(k) -= temp * res(ct + order);
				ct++;
			}
		}
	}
	return res;
}

template<index_t order>
Eigen::Matrix<numeric_t, -1, 2> grad_bi_1_tria(const coords_t & x){
	
	const Eigen::Matrix<numeric_t, -1, 2> sf_high = grad_bi_tria<order>(x);
	const Eigen::Matrix<numeric_t, -1, 2> sf_low = grad_bi_tria<order - 1>(x);
	const index_t n_dofs = (order + 1) * (order + 2) / 2 - 1;
	
	Eigen::Matrix<numeric_t, -1, 2> res(n_dofs, 2); 
	res.middleRows<n_dofs - order>(order) = sf_high.middleRows<n_dofs - order>(order + 1);
	res.topRows<order>() = sf_low.topRows<order>();
	
	const nodes_t dofs_high = get_ref_dofs<order, 3>();
	const nodes_t dofs_low = get_ref_dofs<order - 1, 3>();
	
	const index_t n_corr_fs = n_dofs - order - 1;
	
	for(index_t k = 0; k < order; ++k){
		numeric_t norm_fac = non_normal_tria_bf<order - 1>(0, k, dofs_low.col(k));
		index_t ct = 0;
		for(index_t i = 1; i < order; ++i){
			for(index_t j = 0; j < order + 1 - i; ++j){
				const numeric_t temp = non_normal_tria_bf<order - 1>(0, k, dofs_high.col(ct + order + 1)) / norm_fac;
				res.row(k) -= temp * res.row(ct + order);
				ct++;
			}
		}
	}
	return res;
}









