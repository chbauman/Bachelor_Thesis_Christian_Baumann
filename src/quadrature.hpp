typedef unsigned short n_quad_p_t;
typedef Eigen::Matrix<numeric_t, -1, 1> quad_weights_t;
typedef Eigen::Matrix<numeric_t, -1, 2> quad_points_t;

struct quadRule {
	const el_type_t el_type;
	const n_quad_p_t n;
	const quad_weights_t weights;
	const quad_points_t points;
	quadRule(const el_type_t el_type, n_quad_p_t n, quad_weights_t weights,
					const quad_points_t points): el_type(el_type), n(n), 
					weights(weights), points(points) {};
};

typedef unsigned short quad_ord_t;

#include "quadrature_tria.hpp"
#include "quadrature_quad.hpp"

template<index_t pol_order, index_t el_type>
constexpr index_t quad_ord_to_use(){
	return (el_type == 4 ? pol_order : 2 * pol_order - 1);
};
