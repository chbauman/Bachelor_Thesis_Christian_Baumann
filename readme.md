# Installation instructions

## Prerequisites:
	-Eigen
	-gcc compiler
	-python (for plotting only)
	-openMP

In the 'build' folder in the 'Makefile', change
the Eigen include path, e.g. add
... -I/path/to/eigen/ ...
then just execute 
	
	make

to compile and run the 'main.cpp' file.
This file will conduct the convergence studies
of the original problem, it will also produce the 
correspondingplots and save them 
in the 'Plots' folder. If 

	make exec_validation
	
is executed, then the file 'main_validation.cpp' 
will be compiled and run. This will conduct the 
convergence analysis on the unit disk 
for validation of the code, the plots will be stored
as in the above case. If a new file
with name 'main_new_file.cpp' is added having the
same structure as the two above, then it can be
compiled and run using 
	
	make exec_new_file

The folder 'Data' contains the data files used for
plotting. The folder 'src' contains the core part
of the code. In 'Docs' there are some plotting files
and a '.cpp' file that produces plots of the shape 
functions and other plots appearing in the thesis.
Folder 'Aux' contains the code that writes the 
code for the quadrature and the code that produces 
the code for the low order shape functions.
