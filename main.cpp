
#define VISU
#define USE_DIRECT

#include "src/header.hpp"


int main(){
	std::cout << std::setprecision(16);
	
	// Get the deformations
	std::vector<Deformation> defs_to_try = get_deformations_vector();
	
	// Initialize meshes
	const index_t n_meshes = 8;
	index_t num_dofs;
	index_t num_boundary_dofs;
	nodes_t nodes;
	std::vector<mesh> meshes;
	std::vector<nodes_t> nodes_vec(n_meshes);
	std::vector<index_t> num_dofs_vec(n_meshes);
	std::vector<index_t> num_boundary_dofs_vec(n_meshes);
	for(index_t i = 1; i <= n_meshes; ++i){
		//add_to_meshes(meshes, nodes_vec, num_dofs_vec, num_boundary_dofs_vec, RadialMeshWrapper((1<<i)-1,6*(1<<i-1), 3));
		add_to_meshes(meshes, nodes_vec, num_dofs_vec, num_boundary_dofs_vec, HPMeshWrapper(i,6));
		//add_to_meshes(meshes, nodes_vec, num_dofs_vec, num_boundary_dofs_vec, TriangularMeshWrapper(3*i, 4*i));
		//add_to_meshes(meshes, nodes_vec, num_dofs_vec, num_boundary_dofs_vec, RegTriaMeshWrapper(i, 6, 1));
	}
	
	// Initialize output vectors for plots
	vector_t h1_errs(n_meshes);
	vector_t l2_errs(n_meshes);
	vector_t n_dofs(n_meshes);
	vector_t n_els(n_meshes);
	
	
	for(index_t def_ind = 0; def_ind < defs_to_try.size(); ++def_ind){
		for(index_t mesh_id = 0; mesh_id < n_meshes; ++mesh_id){
			
			mesh final_mesh = meshes[mesh_id];
			num_dofs = num_dofs_vec[mesh_id];
			num_boundary_dofs = num_boundary_dofs_vec[mesh_id];
			nodes = nodes_vec[mesh_id];
			
			std::cout << "\nUsing mesh with: \n";
			std::cout << "Number of elements: " << final_mesh.elements.size() << "\n";
			std::cout << "Number of dofs: " << num_dofs << "\n";
			std::cout << "On boundary: " << num_boundary_dofs << "\n\n";
			
			// Define parameters
			const index_t N = defs_to_try[def_ind].n;
			Eigen::Matrix<numeric_t, -1, 1> s, c, y, z;
			s = defs_to_try[def_ind].s;
			c = defs_to_try[def_ind].c;
			y = defs_to_try[def_ind].y;
			z = defs_to_try[def_ind].z;
			
			auto r = [&](numeric_t phi){
				numeric_t res = 1.0;
				for(index_t i = 0; i < N; ++i){
					res += c(i) * y(i) * std::cos((i + 1) * phi) + s(i) * z(i) * std::sin((i + 1) * phi);
				};
				return res;
			};
			
			// Verify conditions on s, c, y and z
			if(s.cwiseAbs().sum() + c.cwiseAbs().sum() > 0.5 || 
							y.cwiseAbs().maxCoeff() > 1.0 || z.cwiseAbs().maxCoeff() > 1.0) 
				std::cout << "Parameters do not fulfill conditions.\n\n";
			
			// Specify boundary conditions
			auto bcx = [&](coords_t x){
				numeric_t phi = atan2(x(1), x(0));
				return (r(phi) - 1) * std::cos(phi);
			};
			auto bcy = [&](coords_t x){
				numeric_t phi = atan2(x(1), x(0));
				return (r(phi) - 1) * std::sin(phi);
			};
			
			vector_t ux(num_boundary_dofs), uy(num_boundary_dofs);
			for(index_t i = 0; i < num_boundary_dofs; ++i){
				ux(i) = bcx(nodes.col(i));
				uy(i) = bcy(nodes.col(i));		
			}
			std::cout << "Boundary vectors and parameters initialized.\n";
			
			// Assemble matrix
			std::cout << "\nAssembling matrix for auxiliary problem... \n";
			sparseMatrix_t A00(num_dofs-num_boundary_dofs, num_dofs-num_boundary_dofs);
			sparseMatrix_t A0d(num_dofs-num_boundary_dofs, num_boundary_dofs);
			sparseMatrix_t Add(num_boundary_dofs, num_boundary_dofs);
			const Info emp_inf;
			assemble<2>(A00, A0d, Add, final_mesh, nodes, emp_inf);
			std::cout << "Done.\n";
			
			// Solve sparse system
			std::cout << "\nSolving LSE for auxiliary problem... \n";
			solver_t solver;
			solver.compute(A00);
			const vector_t wx = solver.solve(-A0d*ux);
			const vector_t wy = solver.solve(-A0d*uy);
			std::cout << "Done.\n";
			
			// Transform dofs using computed transformation w
			std::cout << "\nTransforming dofs... \n";
			nodes_t nodes_def = nodes;
			// First the boundary
			for(index_t i = 0; i < num_boundary_dofs; ++i){
				nodes_def(0,i) += ux(i);
				nodes_def(1,i) += uy(i);
			};
			// Then the interior nodes_def
			for(index_t i = num_boundary_dofs; i < num_dofs; ++i){
				nodes_def(0,i) += wx(i - num_boundary_dofs);
				nodes_def(1,i) += wy(i - num_boundary_dofs);
			};	
			std::cout << "Done.\n";
#ifdef VISU
			// Visualize the deformed mesh
			std::cout << "\nVisualizing... \n";
			const std::string dat_path = "../Data/";
			const std::string plt_path = "../Plots/";
			const std::string def_mesh_file_name = std::string("deformed_mesh_") + 
									defs_to_try[def_ind].name + std::string("_") + 
									std::to_string(mesh_id);
			visualize_mesh_generic(final_mesh, nodes_def, dat_path + def_mesh_file_name);
			system(std::string("python ../mesh_visu.py " + dat_path + def_mesh_file_name + " "+ std::to_string(2 * final_mesh.elements.size()) + " 0 " + plt_path + def_mesh_file_name + ".pdf 0").c_str());
			std::cout << "Done visualizing " << def_mesh_file_name << ".\n";
#endif
			
			// Specify boundary conditions for final problem
			std::cout << "\nInitializing BCs for final problem... \n";
			vector_t u0(num_boundary_dofs);
			auto fct = [](coords_t x){
				return std::exp(x(1))*std::sin(x(0));
			};		
			auto exact_sol_grad = [](coords_t x){
				coords_t res;
				res << std::exp(x(1))*std::cos(x(0)), std::exp(x(1))*std::sin(x(0));
				return res;
			};
			for(index_t i = 0; i < num_boundary_dofs; ++i){
				u0(i) = fct(nodes_def.col(i));
			}
			std::cout << "Done.\n";
			
			// Assemble matrices for final problem			
#ifdef USE_DIRECT
			std::cout << "\nAssembling matrix for final problem directly... \n";
			nodes_t w_vec(2, num_dofs);
			w_vec << ux.transpose(), wx.transpose(), uy.transpose(), wy.transpose();
			const Info inf = Info(w_vec);
			assemble<1>(A00, A0d, Add, final_mesh, nodes, inf);
			std::cout << "Done.\n\nSolving LSE... \n";
			solver.compute(A00);
			const vector_t mu_dir = solver.solve(-A0d*u0);
			std::cout << "Done.\n";
#endif
			std::cout << "\nAssembling matrix for final problem using deformed mesh... \n";
			assemble<0>(A00, A0d, Add, final_mesh, nodes_def, emp_inf);	
			std::cout << "Done.\n\nSolving LSE... \n";
			solver.compute(A00);
			const vector_t mu = solver.solve(-A0d*u0);
			std::cout << "Done.\n";
			
			// Compute the errors
			std::cout << "\nComputing errors... \n";
			vector_t full_sol(num_dofs);
			full_sol << u0, mu;
			full_sol << u0, mu_dir;
			compute_h1_error(final_mesh, nodes_def, full_sol, exact_sol_grad, fct, h1_errs(mesh_id), l2_errs(mesh_id));
			n_dofs(mesh_id) = num_dofs;
			n_els(mesh_id) = final_mesh.numEls;
			std::cout << "Done.\n";
		};
		
		std::string data_path = "../Data/";
		std::string ext = "_cvg_study_def_" + defs_to_try[def_ind].name;
		writeToFile(data_path + "h1_errs" + ext + ".txt", h1_errs);
		writeToFile(data_path + "l2_errs" + ext + ".txt", l2_errs);
		writeToFile(data_path + "n_dofs" + ext + ".txt", n_dofs);
		writeToFile(data_path + "n_els" + ext + ".txt", n_els);
		system(std::string("python ../plot_error.py " + ext).c_str());
		std::cout << "Done with deformation " << defs_to_try[def_ind].name << ".\n";
	}
	return 0;
}
